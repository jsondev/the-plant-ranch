<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'plantran_alpha');

/** MySQL database username */
define('DB_USER', 'plantran_plantdb');

/** MySQL database password */
define('DB_PASSWORD', 'i6Csa~UpK^JR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>|$:r6V0H,bs03^|-.L~yM1K*4}U&bu=+Jf}X#)Q.I<KX&<4I6rZ3u7|O@hn%>^`');
define('SECURE_AUTH_KEY',  'T}:/oZ)3 B1`/Mh0A9osj&?MQt?yY=G-%F&*1r~]=L}[tQ+HW-#z+xNXR1#(VoD?');
define('LOGGED_IN_KEY',    'HP8C^~=0y3|[,VSD`SG&rMI|d27>Y}p/(3}5uHLh{J-PNv;eBbnH6n`XQl;qa@$p');
define('NONCE_KEY',        '20mgw-Y?+-Vh2vybqTlguMLIXer+jrCeBqqq+&NK+v?-&!BvcJ[,3{/zU=Q(F3vD');
define('AUTH_SALT',        'u<s_b_rXyos-s-@]5l(}-kT3}a72tEH#mu_/G]EB2PS`719V8I5^mJ-@M-zg,SQg');
define('SECURE_AUTH_SALT', 'q=u|wuO0@@.Tyg(zWNf[vi ozsTnH^(N/G}BVFa_PZmcaxm;k_nF-*b9]n54D3c2');
define('LOGGED_IN_SALT',   'sQO)du1wBCQP$8`f`E)edrbRQJ&XO7nrErEc{>)]3Xe.]cB[`xA8#R|:Kl*fB0WJ');
define('NONCE_SALT',       'd!/.yMy|k.n#3!Up|ey]RD%-}-ZZoD %2}:dm7D>)fM7fa<v-dm?G4)|Qinyj,gD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
