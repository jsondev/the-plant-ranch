<?php 
/**
 * Template Name: Services Page 
 */
    ?>
    <?php get_header(); ?>

    <section class="section darkbg fullscreen paralbackground parallax" style="background-image:url('upload/slider_01.jpg');" data-img-width="1793" data-img-height="768" data-diff="100">
            <div class="overlay lightoverlay"></div>
            <div class="container">
                <div class="title-area text-center">
                    <h2><?php the_title(); ?></h2>
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo home_url(); ?>">Home</a></li>
                            <li class="active">Services</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </section><!-- end page-title -->

        <section class="section grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                            <div class="tabbed-widget">
                                <ul class="nav nav-tabs row nopadding">
                                    <li class="active col-md-3 col-sm-6 col-xs-12 nopadding"><a data-toggle="tab" href="#home">garden fence</a></li>
                                    <li class="col-md-3 col-sm-6 col-xs-12 nopadding"><a data-toggle="tab" href="#menu1">garden watering</a></li>
                                    <li class="col-md-3 col-sm-6 col-xs-12 nopadding"><a data-toggle="tab" href="#menu2">preparing landscape</a></li>
                                    <li class="col-md-3 col-sm-6 col-xs-12 nopadding"><a data-toggle="tab" href="#menu3">garden supplies</a></li>
                                </ul>

                                <div class="tab-content row">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="row">
                                            <div id="videoyoutube" class="col-md-5">
                                                <iframe width="370" height="208" src="https://www.youtube.com/embed/eKFTSSKCzWA" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div class="col-md-7">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <p><img src="images/icons/icon_02.png" class="alignleft" alt="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquaadipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                    <div id="menu2" class="tab-pane fade">
                                        <p><img src="images/icons/icon_03.png" class="alignright" alt="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut laborLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquae et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                    <div id="menu3" class="tab-pane fade">
                                        <p><img src="images/icons/icon_04.png" class="alignleft" alt="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                            </div><!-- end tabbed-widget -->
                    </div>
                </div>
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section white">
            <div class="container">
                <div class="row module-wrapper text-center">
<?php if( have_rows('module') ):
                                                // loop through the rows of data
                                while ( have_rows('module') ) : the_row();  ?>

                    <div class="col-md-3 col-sm-3 why-us">
                        <img src="<?php the_sub_field('module_image'); ?>" alt="" class="wow fadeIn">
                        <strong><?php the_sub_field('module_caption'); ?></strong> 
                        <p><?php the_sub_field('module_content'); ?></p>
                    </div><!-- end col -->
                    <?php
                    endwhile;
                    else :
                    // no rows found
                        endif; ?>
                    <!-- end col -->
                </div><!-- end module-wrapper -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section grey">
            <div class="container">
                    <div id="owl-sticky" class="row sticky-row">
                    <?php if( have_rows('slider_module') ):
                                                // loop through the rows of data
                                while ( have_rows('slider_module') ) : the_row();  ?>

                        <div class="sticky-col">
                            <div class="imageWrapper">
                                <img src="<?php the_sub_field('slider_image'); ?>" alt="" class="img-responsive">
                            </div>
                            <div class="big-meta">  
                                <h3><a href="<?php the_sub_field('slider_url'); ?>"><?php the_sub_field('slider_caption'); ?></a></h3>
                            </div>
                        </div>

                                            <?php
                    endwhile;
                    else :
                    // no rows found
                        endif; ?>

                       
                    </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section white">
            <div class="container">
                <div class="row service-style-2 text-center">
                    <div class="col-md-4 col-sm-6">
                        <div class="services-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                            <img src="images/icons/mini_icon_01.png" alt="">
                            <h4>tree planting</h4>
                            <p>Odec adesterc timeam toruatos, eam debitis sententiae examles.Cure sed homerous alienum.Cu sed homero alienum lobortis</p>
                        </div><!-- end custom-module -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6">
                        <div class="services-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                            <img src="images/icons/mini_icon_02.png" alt="">
                            <h4>Beekeeping</h4>
                            <p>Odec adesterc timeam toruatos, eam debitis sententiae examles.Cure sed homerous alienum.Cu sed homero alienum lobortis</p>
                        </div><!-- end custom-module -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6">
                        <div class="services-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                            <img src="images/icons/mini_icon_03.png" alt="">
                            <h4>irrigation</h4>
                            <p>Odec adesterc timeam toruatos, eam debitis sententiae examles.Cure sed homerous alienum.Cu sed homero alienum lobortis</p>
                        </div><!-- end custom-module -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <?php get_footer(); ?>