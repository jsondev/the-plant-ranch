<?php 
/*
  Template Name: Services 
 */?>

 <?php get_header(); ?>

 <section class="section darkbg fullscreen paralbackground parallax" style="background-image:url('<?php the_field('top_image'); ?>');" data-img-width="1793" data-img-height="768" data-diff="100">
    <div class="overlay lightoverlay"></div>
    <div class="container">
        <div class="title-area text-center">
            <h2><?php the_title(); ?></h2>
            <div class="bread">
                <ol class="breadcrumb">
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li class="active"><?php the_title(); ?></li>
                </ol>
            </div><!-- end bread -->
        </div><!-- /.pull-right -->
    </div>
</section><!-- end page-title -->

<section class="section white">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-8 page-content">
                <?php the_field('page_content'); ?>
                <div class="accordion-widget">
                    <div class="accordion-toggle-2">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <?php if( have_rows('accordion') ):
                                                // loop through the rows of data
                                while ( have_rows('accordion') ) : the_row();  ?>
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour">
                                            <h3><?php the_sub_field('title'); ?><i class="indicator fa fa-plus"></i></h3>
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <?php the_sub_field('information'); ?>
                                    </div>
                                </div>
                                <?php
                                endwhile;
                                else :
                    // no rows found
                                    endif; ?>
                            </div>
                        </div>
                    </div>
                </div><!-- accordion -->

            </div><!-- end accordion-widget -->  
            <?php get_sidebar(); ?>
        </div>

    </div>

    <!-- end container -->
</section><!-- end section -->

<?php get_footer(); ?>