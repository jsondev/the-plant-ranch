    (function($) {
      "use strict";
/* ==============================================
MAP -->
=============================================== */

  var locations = [
    ['<div class="infobox"><h3 class="title"><a href="https://www.google.com/maps/dir/Current+Location/5909%2013th%20Ave,%20Regina,%20SK%20S4W%201A9" target="_blank">Our Location</a></h3><span>5909 13th Ave<br>Regina, SK<br>S4W 1A9</span></p></div></div></div>', 50.4448467,-104.6999133, 2]
    ];
  
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      scrollwheel: false,
      navigationControl: true,
      mapTypeControl: false,
      scaleControl: false,
      draggable: true,
      
      center: new google.maps.LatLng(50.4448467,-104.6999133),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
  
    var infowindow = new google.maps.InfoWindow();
  
    var marker, i;
  
    for (i = 0; i < locations.length; i++) {  
    
      marker = new google.maps.Marker({ 
      position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
      map: map ,
      icon: 'http://www.plantranch.ca/production/wp-content/uploads/2016/01/marker.png'
      });
  
  
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }

    })(jQuery);