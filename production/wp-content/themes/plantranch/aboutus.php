<?php 
/*
  Template Name: About Us
 */?>

 <?php get_header(); ?>

 <section class="section darkbg fullscreen paralbackground parallax" style="background-image:url('<?php the_field('top_image'); ?>');" data-img-width="1793" data-img-height="768" data-diff="100">
    <div class="overlay lightoverlay"></div>
    <div class="container">
        <div class="title-area text-center">
            <h2><?php the_title(); ?></h2>
            <div class="bread">
                <ol class="breadcrumb">
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li class="active"><?php the_title(); ?></li>
                </ol>
            </div><!-- end bread -->
        </div><!-- /.pull-right -->
    </div>
</section><!-- end page-title -->

        <section class="section white">
            <div class="container about-us">
                <div class="general-title text-center">
                    <h4><?php the_field('welcome_title'); ?></h4>
                    <p class="lead"><?php the_field('welcome_title_motto'); ?></p>
                    <hr>
                </div><!-- end general title -->
                <div class="text-center">
                    <p><?php the_field('main_content'); ?></p>
                </div>
                <hr class="invis">
                <div class="general-title text-center">
                    <h4><?php the_field('team_title'); ?></h4>
                    <p class="lead"><?php the_field('team_motto'); ?></p>
                    <hr>
                </div><!-- end general title -->
                <div class="row module-wrapper text-center">
                   <?php if( have_rows('team_repeater') ):
                                                // loop through the rows of data
                                while ( have_rows('team_repeater') ) : the_row();  ?>
                     <div class="col-md-3 col-md-offset-2 col-sm-6 team-member">
                        <div class="entry">
                            <img class="img-responsive" src="<?php the_sub_field('member_photo'); ?>" alt="">
                        </div><!-- end entry -->
                        <h4><?php the_sub_field('member_name'); ?></h4>
                        <p><?php the_sub_field('bio'); ?></p>
                    </div><!-- end <?php the_sub_field('member_name'); ?> -->
                                <?php
                                endwhile;
                                else :
                    // no rows found
                               endif; ?>
                       </div><!-- row -->
                <hr class="invis">
                </div><!-- end container -->
        </section><!-- end section -->
<?php get_footer(); ?>