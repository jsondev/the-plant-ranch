<?php
    /**
     * The functions file is used to initialize everything in the theme.  It controls how the theme is loaded and
     * sets up the supported features, default actions, and default filters.  If making customizations, users
     * should create a child theme and make changes to its functions.php file (not this one).  Friends don't let
     * friends modify parent theme files. ;)
     *
     * Child themes should do their setup on the 'after_setup_theme' hook with a priority of 11 if they want to
     * override parent theme features.  Use a priority of 9 if wanting to run before the parent theme.
     *
     * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
     * General Public License as published by the Free Software Foundation; either version 2 of the License,
     * or (at your option) any later version.
     *
     * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
     * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
     *
     * You should have received a copy of the GNU General Public License along with this program; if not, write
     * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
     *
     * @package plantranch
     * @subpackage Functions
     * @version 1.0
     * @author James Anderson <James.Anderson@innovativeimaginations.com>
     * @copyright Copyright (c) 2015 - 2016, James Anderson
     * @link http://imaginationeverywhere.info/themes/belife
     * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
     */

    /* Load the core theme framework. */
    require_once( trailingslashit( get_template_directory() ) . 'library/hybrid.php' );
    new Hybrid();

    /* Do theme setup on the 'after_setup_theme' hook. */
      add_action( 'after_setup_theme', 'plantranch_setup', 10);

    if (!function_exists('plantranch_setup')){

        /**
         * plantranch Theme setup.
         *
         * Set up theme defaults and registers support for various WordPress features.
         *
         * Note that this function is hooked into the after_setup_theme hook, which
         * runs before the init hook. The init hook is too late for some features, such
         * as indicating support post thumbnails.
         *
         * @since Fat Cat Media House WordPress Theme 1.0
         */

        function plantranch_setup(){
            /* Add theme support for framework extensions. */
            
            /*  This is to activate the featured image in Posts
             *  The best thumbnail/image script ever. 
             */
            add_theme_support( 'get-the-image' );

            /* Enable custom template hierarchy. */
            add_theme_support( 'hybrid-core-template-hierarchy' );

            /* Load shortcodes. */
            add_theme_support( 'hybrid-core-shortcodes' );

            /*  Adds input fields in the post editor for adding post-specific 
             *  meta information as well as sets up some defaults on other pages.
             */
            add_theme_support( 'hybrid-core-seo' );

            /*  This theme uses wp_nav_menu() in one location. */
            register_nav_menus( array(
                'front_page_menu' => __( 'Front Page Menu', 'plantranch'),
                'default_page_menu' => __( 'Default Page Menu', 'plantranch'),
            ));

            /*  Here are Hybrid Core Framework Extensions which provide more 
             *  Theme Supports
             */
            
            /* Breadcrumbs. Yay! */
            add_theme_support( 'breadcrumb-trail' );
            
            remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
            remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
            remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
            remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
            remove_action( 'wp_head', 'index_rel_link' ); // index link
            remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
            remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
            remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
            remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP ver
            
            // Hook into the header to add the needed meta 
            add_action('wp_head', 'add_meta_tags', 0);

            // Hook into the header to add the needed meta 
            add_action( 'wp_head', 'add_link_tags', 2);

            // Hook into the 'wp_enqueue_scripts' action
            add_action( 'wp_enqueue_scripts', 'plantranch_theme_scripts' );
        
            // Enqueue Scripts in the Admin
            //add_action( 'admin_enqueue_scripts', 'plantranch_post_listing_column_resize' );       
            
            //Set Image Sizes
            //add_action( 'init', 'text_domain_add_image_sizes' );
            
            /* Filters hooks go here. */

            //enable automatic updates for all plugins
            add_filter( 'auto_update_plugin', '__return_true' );

            // enable automatic updates for all themes
            add_filter( 'auto_update_theme', '__return_true' );
            
            /** Gravity Forms Filters 
             *  These functions are located in includes/forms.php  
            **/
            // Add custom classes to inputs
            //add_action("gform_field_input", "gf_custom_class", 10, 5);
 
            /** Gravity Forms Filters 
              * These functions are located in includes/forms.php    
            **/

            // This function forces jQuery calls to be loaded in the footer after all other scripts
            add_filter("gform_init_scripts_footer", "init_scripts");

            // filter the Gravity Forms button type 
            //add_filter("gform_submit_button", "form_submit_button", 10, 2);

            // Add a class to menu icon
            //add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
            
            //Add Prev & Next Link Styled Paginaiton
            add_filter('next_posts_link_attributes', 'next_page_posts_link_attributes');
            add_filter('previous_posts_link_attributes', 'previous_page_posts_link_attributes');

            /**
             * Display column based on Role
             *
             * The column name can be found by opening the settings screen of your column
             * and hover your mouse over "Type". In the tooltip you will find the column Name.
             *
             * In this example we will disable the Custom Field column for the 'subscriber' role
             *
             */
            //add_filter( "manage_edit-post_columns", "myplugin_filter_hide_column_based_on_role", 101, 1 );
            //add_filter( "manage_edit-page_columns", "myplugin_filter_hide_column_based_on_role", 101, 1 );

            
            /**
            * Create Advance Custom Fields Options Pages.
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/options-pages.php');

            /**
            * Register our sidebars and widgetized areas.
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/dynamic-sidebars.php');

            /**
            * Create Breadcrumbs
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/breadcrumbs.php');

            /**
            * Gravity Forms Configuration
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/forms.php');

            /**
            * Create Pagination
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/pagination.php');

            /**
            * Customize Nav Menu
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/walker-menu.php');
        }
    }
    
    //adds meta tags along with the title tag to the header.php file
    if (!function_exists('add_meta_tags')) {
        function add_meta_tags() {
        ?>
            
        <?php
        }
    }

    // Add the appropriate link tags to the header
    if(!function_exists('add_link_tags')){
        function add_link_tags(){

        }
    }

    if (!function_exists('the_slug')) {
        // Get the slug
        function the_slug() {
            $post_data = get_post($post->ID, ARRAY_A);
            $slug = $post_data['post_name'];
            return $slug; 
        }
    }
    
    if (!function_exists('special_nav_class')) {
        function special_nav_class($classes, $item){
             if( in_array('menu-item', $classes) ){
                     $classes[] = 'dropdown';
             }
             return $classes;
        }  
    }

    //Set Image Sizes
    if (!function_exists('plantranch_add_image_sizes')) {
        function plantranch_add_image_sizes() {
           add_image_size('featured-image-name', width, height, true/false);
        }
    }

    /**
     * Display column based on Role
     *
     * The column name can be found by opening the settings screen of your column
     * and hover your mouse over "Type". In the tooltip you will find the column Name.
     *
     * In this example we will disable the Custom Field column for the 'subscriber' role
     *
     */
    function myplugin_filter_hide_column_based_on_role( $column_headings ) {

        // Disable the custom field column with Name 'column-meta' for subscribers
        if ( current_user_can( 'subscriber' ) ) {
            unset( $column_headings[ 'column-meta' ] );
        }

        return $column_headings;
    }

    // Register Script
    if(!function_exists('plantranch_theme_scripts')){
        function plantranch_theme_scripts(){
            global $wp_scripts;
            global $wp_styles;
            
// Settings
            wp_deregister_style( 'settings' );
            wp_register_style( 'settings',trailingslashit( THEME_URI ) .'rs-plugin/css/settings.css', false, '1.0');
            wp_enqueue_style( 'settings' );
// Animate
            wp_deregister_style( 'animate' );
            wp_register_style( 'animate',trailingslashit( THEME_URI ) .'css/animate.css', false, '1.0');
            wp_enqueue_style( 'animate' );
// Twitter Bootstrap
            wp_deregister_style( 'bootstrap' );
            wp_register_style( 'bootstrap',trailingslashit( THEME_URI ) .'css/bootstrap.css', false, '1.0');
            wp_enqueue_style( 'bootstrap' );
// Carousel
            wp_deregister_style( 'carousel' );
            wp_register_style( 'carousel',trailingslashit( THEME_URI ) .'css/carousel.css', false, '1.0');
            wp_enqueue_style( 'carousel' );
// Bootstrap Select
            wp_deregister_style( 'bootstrap-select' );
            wp_register_style( 'bootstrap-select',trailingslashit( THEME_URI ) .'css/bootstrap-select.css', false, '1.0');
            wp_enqueue_style( 'bootstrap-select' );
// jQuery UI
            wp_deregister_style( 'jquery-ui' );
            wp_register_style( 'jquery-ui',trailingslashit( THEME_URI ) .'css/jquery-ui.css', false, '1.0');
            wp_enqueue_style( 'jquery-ui' );
// Main WordPress Style CSS
            wp_deregister_style( 'style' );
            wp_register_style( 'style',trailingslashit( THEME_URI ) .'style.css', false, '1.0');
            wp_enqueue_style( 'style' );
// Custom Style Sheet    
            wp_deregister_style( 'custom' );
            wp_register_style( 'custom',trailingslashit( THEME_URI ) .'css/custom.css', false, '1.0');
            wp_enqueue_style( 'custom' );
    

            // wp_deregister_style( '' );
            // wp_register_style( '',trailingslashit( THEME_URI ) .'', false, '1.0' );
            // wp_enqueue_style( '' );
            //Conditonally add styles if necessary
            //wp_style_add_data( '', 'conditional', 'lt IE 9' );
            //wp_style_add_data( '', 'conditional', 'gte IE 9' );
    
            // Load our main stylesheet.
            // wp_enqueue_style( '', get_stylesheet_uri(), array() );
         




// HTML 5 Shiv
    wp_deregister_script( 'html5shiv' );
    wp_register_script( 'html5shiv','https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js', false, '1.0');
    wp_enqueue_script( 'html5shiv' );
// Respond
    wp_deregister_script( 'respond' );
    wp_register_script( 'respond','https://oss.maxcdn.com/respond/1.4.2/respond.min.js', false, '1.0');
    wp_enqueue_script( 'respond' );
        
        
         
             //Use the wp scripts constant if necesarry
             //global $wp_scripts;
             //Conditonally add scripts if necessary
             //$wp_scripts->add_data( '', 'conditional', 'lt IE 9' );
             //$wp_scripts->add_data( '', 'conditional', 'gte IE 9' );


wp_deregister_script( 'jquery' );
wp_register_script( 'jquery',trailingslashit( THEME_URI ) .'js/jquery.min.js', false, '1.0', true);
wp_enqueue_script( 'jquery' );
    
wp_deregister_script( 'bootstrapjs' );
wp_register_script( 'bootstrapjs',trailingslashit( THEME_URI ) .'js/bootstrap.js', false, '1.0', true);
wp_enqueue_script( 'bootstrapjs' );
    
wp_deregister_script( 'retina' );
wp_register_script( 'retina',trailingslashit( THEME_URI ) .'js/retina.js', false, '1.0', true);
wp_enqueue_script( 'retina' );
    
wp_deregister_script( 'parallax' );
wp_register_script( 'parallax',trailingslashit( THEME_URI ) .'js/parallax.js', false, '1.0', true);
wp_enqueue_script( 'parallax' );
    
wp_deregister_script( 'wow' );
wp_register_script( 'wow',trailingslashit( THEME_URI ) .'js/wow.js', false, '1.0', true);
wp_enqueue_script( 'wow' );
    
    wp_deregister_script( 'carouseljs' );
    wp_register_script( 'carouseljs',trailingslashit( THEME_URI ) .'js/carousel.js', false, '1.0', true);
    wp_enqueue_script( 'carouseljs' );
        
wp_deregister_script( 'customjs' );
wp_register_script( 'customjs',trailingslashit( THEME_URI ) .'js/custom.js', false, '1.0', true);
wp_enqueue_script( 'customjs' );

wp_deregister_script( 'jquerythemepunch' );
wp_register_script( 'jquerythemepunch',trailingslashit( THEME_URI ) .'rs-plugin/js/jquery.themepunch.tools.min.js', false, '1.0', true);
wp_enqueue_script( 'jquerythemepunch' );
    
    wp_deregister_script( 'jquerythemepunchrev' );
    wp_register_script( 'jquerythemepunchrev',trailingslashit( THEME_URI ) .'rs-plugin/js/jquery.themepunch.revolution.min.js', false, '1.0', true);
    wp_enqueue_script( 'jquerythemepunchrev' );
        
wp_deregister_script( 'jqueryuijs' );
wp_register_script( 'jqueryuijs',trailingslashit( THEME_URI ) .'js/jquery-ui.js', false, '1.0', true);
wp_enqueue_script( 'jqueryuijs' );
    
    wp_deregister_script( 'jqueryuitimepicker' );
    wp_register_script( 'jqueryuitimepicker',trailingslashit( THEME_URI ) .'js/jquery-ui-timepicker-addon.js', false, '1.0', true);
    wp_enqueue_script( 'jqueryuitimepicker' );
        
wp_deregister_script( 'bootstrapselect' );
wp_register_script( 'bootstrapselect',trailingslashit( THEME_URI ) .'js/bootstrap-select.js', false, '1.0', true);
wp_enqueue_script( 'bootstrapselect' );

            wp_deregister_script( 'googlemaps' );
            wp_register_script( 'googlemaps','http://maps.google.com/maps/api/js?sensor=false', false, '1.0', true);
            wp_enqueue_script( 'googlemaps' );

            wp_deregister_script( 'jsmap' );
            wp_register_script( 'jsmap',trailingslashit( THEME_URI ) .'js/map.js', false, '1.0', true);
            wp_enqueue_script( 'jsmap' );


    }
}