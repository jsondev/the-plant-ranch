<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package text-doman
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<?php if ( have_comments() ) : ?>

	<?php //if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :  are there comments to navigate through ?>
		<!--<nav id="comment-nav-above" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'my-simone' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'my-simone' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'my-simone' ) ); ?></div>
		</nav> #comment-nav-above -->
		<?php //endif;  check for comment navigation ?>

		<div class="blog-comments">
			<?php wp_list_comments( array(
				'walker' => new custom_walker_comment,
				'style' => 'ul',
				'callback' => null,
				'end-callback' => null,
				'type' => 'all',
				'page' => null,
				'avatar_size' => 80
				) ); ?>
			</div><!-- /#comments-container -->

			<?php //if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :  are there comments to navigate through ?>
		<!--<nav id="comment-nav-below" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'text-doman' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'text-doman' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'text-doman' ) ); ?></div>
		</nav> #comment-nav-below -->
		<?php  //endif; check for comment navigation ?>
	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
		?>
	<p class="no-comments"><?php _e( 'Comments are closed.', 'text-doman' ); ?></p>
<?php endif; ?>

<?php
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$fields =  array(
	  'author' => '<div class="col-md-6"><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
	    '" size="30"' . $aria_req . ' class="form-control" placeholder="NAME *" /></div>',

	  'email' => '<div class="col-md-6"><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
	    '" size="30"' . $aria_req . ' class="form-control" placeholder="EMAIL *" /></div>',

	  'url' => '<div class="col-md-6"><input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
	    '" size="30" class="form-control" placeholder="WEBSITE" /></div>',
	);

	$comment_form_args = array(
		'id_form'           => 'commentform',
		'id_submit'         => 'submit',
		'class_submit'      => 'submit',
		'name_submit'       => 'submit',
		'title_reply'       => __( 'Leave a Comment' ),
		'title_reply_to'    => __( 'Leave a Comment to %s' ),
		'cancel_reply_link' => __( 'Cancel Reply' ),
		'label_submit'      => __( 'SUBMIT' ),
		'format'            => 'html5',

		'comment_field' =>  '<div class="col-md-12"><textarea id="comment" name="comment" class="form-control" placeholder="COMMENT *" cols="50" style="height:200px;" aria-required="true">' .
		'</textarea></div>',

		/*'must_log_in' => '<p class="must-log-in">' .
		sprintf(
			__( 'You must be <a href="%s">logged in</a> to post a comment.' ),
			wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
			) . '</p>',

		'logged_in_as' => '<p class="logged-in-as">' .
		sprintf(
			__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
			admin_url( 'profile.php' ),
			$user_identity,
			wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
			) . '</p>',*/

		'comment_notes_before' => '',

		'comment_notes_after' => '',

		'fields' => apply_filters( 'comment_form_default_fields', $fields ),
	);
?>

	<div class="post-comment">
		<div class="row">
			<?php comment_form($comment_form_args); ?>
		</div>
	</div><!-- /#respond-container -->

	<?php /** COMMENTS WALKER */
	class custom_walker_comment extends Walker_Comment {

	    // init classwide variables
	    var $tree_type = 'comment';
	    var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );
	 
	    /** CONSTRUCTOR
	     * You'll have to use this if you plan to get to the top of the comments list, as
	     * start_lvl() only goes as high as 1 deep nested comments */
	    function __construct() { ?>
	         
	        <h3 id="comments-title">Comments</h3>
	        <ul id="comment-list">
	         
	    <?php }
	     
	    /** START_LVL 
	     * Starts the list before the CHILD elements are added. */
	    function start_lvl( &$output, $depth = 0, $args = array() ) {       
	        $GLOBALS['comment_depth'] = $depth + 1; ?>
	 
	                <ul class="children">
	    <?php }
	 
	    /** END_LVL 
	     * Ends the children list of after the elements are added. */
	    function end_lvl( &$output, $depth = 0, $args = array() ) {
	        $GLOBALS['comment_depth'] = $depth + 1; ?>
	 
	        </ul><!-- /.children -->
	         
	    <?php }
	     
	    /** START_EL */
	    function start_el( &$output, $comment, $depth, $args, $id = 0 ) {
	        $depth++;
	        $GLOBALS['comment_depth'] = $depth;
	        $GLOBALS['comment'] = $comment; 
	        $parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' ); ?>
	         
	        <li <?php comment_class( $parent_class ); ?> id="comment-<?php comment_ID() ?>">
	            <div id="comment-body-<?php comment_ID() ?>" class="comment-body">
	             
	                <div class="comment-author vcard author">
	                    <?php echo ( $args['avatar_size'] != 0 ? get_avatar( $comment, $args['avatar_size'] ) :'' ); ?>
	                    <cite class="fn n author-name"><?php echo get_comment_author_link(); ?></cite>
	                </div><!-- /.comment-author -->
	 
	                <div id="comment-content-<?php comment_ID(); ?>" class="comment-content">
	                    <?php if( !$comment->comment_approved ) : ?>
	                    <em class="comment-awaiting-moderation">Your comment is awaiting moderation.</em>
	                     
	                    <?php else: comment_text(); ?>
	                    <?php endif; ?>
	                </div><!-- /.comment-content -->
	 
	                <div class="comment-meta comment-meta-data">
	                    <a href="<?php echo htmlspecialchars( get_comment_link( get_comment_ID() ) ) ?>"><?php comment_date(); ?> at <?php comment_time(); ?></a> <?php edit_comment_link( '(Edit)' ); ?>
	                </div><!-- /.comment-meta -->
	 
	                <div class="reply">
	                    <?php $reply_args = array(
	                        'add_below' => $add_below, 
	                        'depth' => $depth,
	                        'max_depth' => $args['max_depth'] );
	     
	                    comment_reply_link( array_merge( $args, $reply_args ) );  ?>
	                </div><!-- /.reply -->
	            </div><!-- /.comment-body -->
	 
	    <?php }
	 
	    function end_el(&$output, $comment, $depth = 0, $args = array() ) { ?>
	         
	        </li><!-- /#comment-' . get_comment_ID() . ' -->
	         
	    <?php }
	     
	    /** DESTRUCTOR
	     * I'm just using this since we needed to use the constructor to reach the top 
	     * of the comments list, just seems to balance out nicely:) */
	    function __destruct() { ?>
	     
	    </ul><!-- /#comment-list -->
	 
	    <?php }
	}
