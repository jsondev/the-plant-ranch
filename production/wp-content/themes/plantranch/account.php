<?php 
/*
  Template Name: Account Management
 */?>
<?php get_header(); ?>
<?php if(have_posts()) : ?>
	    <?php while (have_posts()) : the_post(); ?>
        <div class="container-fluid">
            <div class="row">
                   <div class="col-lg-12">
                       <?php the_content(); ?>>
                   </div>
            </div>
			</div>
		

<?php endwhile; ?>
	      
	    <?php else : ?>
	    <?php wp_reset_query(); ?>
	      <h1>No Post Found</h1>
	  	<?php get_search_form( ); ?>

	<?php endif; ?>

<?php get_footer(); ?>