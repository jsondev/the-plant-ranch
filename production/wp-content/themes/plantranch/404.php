<?php get_header(); ?>
 <div class="page-title grey">
            <div class="container">
                <div class="title-area text-center">
                    <h2>Not Found</h2>
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo home_url(); ?>">Home</a></li>
                            <li class="active">Page 404</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

        <section class="section white">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-12">
                    <div class="notfound text-center">
                        <h1>404 - Page Not Found</h1>

                        <blockquote>The page you are looking for no longer exists. Perhaps you can return back to the site's homepage and see if you can find what you are looking for.</blockquote>

                        <a href="<?php echo home_url(); ?>" class="btn btn-primary">Back to Home</a>
                    </div>
                    </div>
                </div>
            </div><!-- end container -->
        </section><!-- end section -->

<?php get_footer(); ?>

