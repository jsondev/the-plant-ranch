<!DOCTYPE html>
<html <?php language_attributes(); ?> >

<head>
	<?php wp_head(); ?>

<div id="loader">
        <div class="loader-container">
            <img src="http://www.plantranch.ca/production/wp-content/uploads/2015/11/load.gif" alt="" class="loader-site spinner">
        </div>
    </div>

        <div id="wrapper">
        <div class="topbar clearfix">
            <div class="container">
                <div class="row-fluid">
                    <div class="col-md-6 text-left">
                        <div class="social">
                            <a href="https://www.facebook.com/Plant-Ranch-Inc-123560576385/" data-tooltip="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>              
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="https://www.linkedin.com/in/lucille-bouvier-2a451840" data-tooltip="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                        </div><!-- end social -->
                    </div><!-- end left -->
                    <div class="col-md-6 text-right">
                        <p>
                            <strong><i class="fa fa-phone"></i></strong> <a href="tel:1-306-525-1352" target="_blank">306-525-1352</a> &nbsp;&nbsp;
                            <strong><i class="fa fa-envelope"></i></strong> <a href="mailto:info.plantranch.ca">info.plantranch.ca</a>
                        </p>
                    </div><!-- end left -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end topbar -->

        <header class="header">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-md-12">
                        <nav class="navbar yamm navbar-default">
                            <div class="container-full">
                                <div class="navbar-table">
                                    <div class="navbar-cell">
                                        <div class="navbar-header">
                                            <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
<img src="http://www.plantranch.ca/production/wp-content/uploads/2016/02/PlantRanchNewlogoFinal.jpeg" alt="Plant Ranch Logo" />
</a>
                                            <div>
                                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="fa fa-bars"></span>
                                                </button>
                                            </div>
                                        </div><!-- end navbar-header -->
                                    </div><!-- end navbar-cell -->

                                    <div class="navbar-cell stretch">
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                            

                                                                <?php 
                    wp_nav_menu( array(
                        'theme_location'    => 'front_page_menu',
                        'container'     => 'div',
                        'container_class' => 'navbar-cell',
                        'menu_class'        => 'nav navbar-nav navbar-right', 
                        'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'walker'        => new custom_theme_walker_nav_menu
                        ) ); 
                        ?>

                                        </div><!-- /.navbar-collapse -->        
                                    </div><!-- end navbar cell -->
                                </div><!-- end navbar-table -->
                            </div><!-- end container fluid -->
                        </nav><!-- end navbar -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </header>

        

	
</head>
<body>