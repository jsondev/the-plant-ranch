<div id="sidebar" class="col-md-4 col-sm-12 col-xs-12">

    <div class="widget">
        <div class="widget-title">
            <h4>Recent posts</h4>
            <hr>
        </div><!-- end title -->

        <div class="recent-post-widget">
            <ul class="recent-posts">
                <?php
                $recent_posts = wp_get_recent_posts();
                foreach( $recent_posts as $recent ){
                    echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
                }
                ?>
            </ul><!-- end latest-tweet -->
        </div><!-- end twitter-widget -->
    </div><!-- end widget -->

    <div class="widget">
        <div class="widget-title">
            <h4>Blog Categories</h4>
            <hr>
        </div><!-- end title -->

        <div class="cats-widget">
            <ul>
                <?php wp_list_categories('orderby=count&order=desc&show_count=1&title_li='); ?>
            </ul><!-- end latest-tweet -->
        </div><!-- end twitter-widget -->
    </div><!-- end widget -->

    <div class="widget">
        <div class="widget-title">
            <?php if ( function_exists( 'wp_tag_cloud' ) ) : ?>
                <h4>Popular Tags</h4>
                <hr>
            </div><!-- end title -->

            <div class="tag-widget">
                <?php wp_tag_cloud( 'smallest=8&largest=22' ); ?>
            <?php endif; ?>
        </div><!-- end text-widget -->
    </div><!-- end widget -->
</div><!-- end content -->