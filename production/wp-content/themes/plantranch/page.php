<?php get_header(); ?>

<?php $query = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 3, 'orderby' => 'menu_order','order' => 'DESC') ); ?>
<?php if($query->have_posts()) : ?>
<?php while ($query->have_posts()) : $query->the_post(); ?>

<h1>There is content here </h1>
<?php the_content(); ?>
<?php endwhile; ?>
<?php else : ?>
<?php wp_reset_query(); ?>
<h1>No Post Found</h1>
<?php get_search_form( ); ?>
<?php endif; ?>




				<?php get_sidebar(); ?>
			</div><!-- end row -->
		</div><!-- end container -->
	</section><!-- end section -->
<?php get_footer(); ?>