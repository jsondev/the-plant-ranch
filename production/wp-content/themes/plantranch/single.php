<?php get_header(); ?>
    <div class="page-title grey">
            <div class="container">
                <div class="title-area text-center">
                    <h2><?php the_title(); ?></h2>
					<div>
						<?php $category = get_the_category();
								$category_title = $category[0]->name; 
								
								?>
						
						<?php if(have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
						<div class="post-meta">
                                  <span>
									  Category: <a href="<?php the_permalink(); ?>" title=""><?php echo $category_title; ?></a>
									  </span>
                                    <span>
									<a href="<?php the_permalink(); ?>"><?php the_tags(); ?></a>
                                    </span>
									<span>
									<a href="<?php the_permalink(); ?>"><?php if(function_exists('the_views')) { the_views(); } ?></a>
                                    </span>         
						</div>
					</div><!-- end desc -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->
<section class="section white">
	<div class="container">
		<div class="row">
			<div id="content" class="single-post col-md-8 col-sm-12 col-xs-12">
				<div class="blog-wrapper">
					
					
					<div class="blog-image">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner" role="listbox">							
                       <?php if( have_rows('slider_repeater') ):
 								$counter = 0;
                                 // loop through the rows of data
                                 while ( have_rows('slider_repeater') ) : the_row();  
 								$counter++;
								 ?>
								 
								 <?php if ($counter === 1): ?>
									 <div class="item active">
 									<img src="<?php the_sub_field('gallery_pictures'); ?>" alt="" class="img-responsive" style="max-height:441px; max-width:750px;"/>
 								</div>
								 
								 <?php else :?>
									 <div class="item">
 									<img src="<?php the_sub_field('gallery_pictures'); ?>" alt="" class="img-responsive" style="max-height:441px; max-width:750px;"/>
 								</div>
								 
 								<?php endif; ?>   
<?php endwhile;
else :
    // no rows found
endif; ?>
							</div>

							<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
								<span class="fa fa-angle-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
								<span class="fa fa-angle-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
                 
					<div class="blog-title">
					<h2 class="section-title"><?php the_field('tagline'); ?></h2>
                        <p>Written by <a href="<?php the_permalink(); ?>"><?php the_author(); ?></a> - <a href="<?php the_permalink(); ?>"><?php the_date(); ?></a></p>
						<?php the_field('content'); ?>
					</div><!-- end desc -->
				</div><!-- end blog-wrapper -->

				

			</div>
			<?php endwhile; ?>

			<?php else : ?>
				<?php wp_reset_query(); ?>
				<h1>No Post Found</h1>
				<?php get_search_form( ); ?>

			<?php endif; ?>


			<!-- end content -->
			<?php get_sidebar(); ?>
		</div><!-- end row -->
	</div><!-- end container -->
</section><!-- end section -->
<?php get_footer(); ?>
