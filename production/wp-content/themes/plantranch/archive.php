<?php get_header(); ?>
<div class="page-title grey">
  <div class="container">
    <div class="title-area text-center">
      <h2><?php single_cat_title() ?></h2>
      <div class="bread">
        <ol class="breadcrumb">
          <li><a href="<?php echo home_url(); ?>">Home</a></li>
          <li class="active">
            <?php single_cat_title() ?>
          </li>
        </ol>
      </div>
      <!-- end bread -->
    </div>
    <!-- /.pull-right -->
  </div>
</div>
<!-- end page-title -->

<section class="section white">
  <div class="container">
    <div class="row">
      <div id="content" class="list-blog col-md-12 col-md-offset-0">
        <?php if(have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="blog-wrapper row">
          <div class="col-md-5">
            <?php if( have_rows('slider_repeater') ):
                            $counter = 0;
                                while( have_rows('slider_repeater') ) : the_row(); 
                                $counter++;
                                ?>
            <div class="blog-image">
              <?php if ($counter === 1): ?>
              <a href="<?php the_permalink(); ?>" title=""><img src="<?php the_sub_field('gallery_pictures'); ?>" alt="" class="img-responsive" style="max-height:269px; max-width:458px;"></a>
              <?php else :?>
              <?php endif; ?>
            </div>
                    <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>
            <!-- end image -->
          </div>
          <div class="col-md-7">
            <div class="blog-title">
              <a class="category_title" href="<?php the_permalink(); ?>" title=""><?php single_cat_title(); ?></a>
              <h2><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h2>
              <div class="post-meta">
                <span>
                                    <i class="fa fa-user"></i>
                                    <a href="<?php the_permalink(); ?>"><?php the_author(); ?></a>
                                    </span>
                <span>
                                    <i class="fa fa-tag"></i>
                                    <a href="<?php the_permalink(); ?>"><?php the_tags(); ?></a>
                                    </span>
                <span>
                                    <span>
                                    <i class="fa fa-clock-o"></i>
                                    <a href="<?php the_permalink(); ?>"><?php the_date(); ?></a>
                                    </span>
                <span>
                                    <i class="fa fa-eye"></i>
                                    <a href="<?php the_permalink(); ?>"><?php if(function_exists('the_views')) { the_views(); } ?></a>
                                    </span>
                <span>
                                    <i class="fa fa-share-alt"></i>
                                    <a href="#">Share</a>
                                    </span>
              </div>
          <?php $summary = get_field('content'); ?>
          <?php echo substr($summary, 0, strpos($summary, ' ', 260)) . "..."; ?>
              <a href="<?php the_permalink(); ?>" class="readmore">Read more</a>
            </div>
            <!-- end desc -->
          </div>
        </div>
        <!-- end blog-wrapper -->
        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>
        <nav class="pagi clearfix">
          <ul class="pagination">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
          </ul>
        </nav>
      </div>
      <!-- end content -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- end section -->
<?php get_footer(); ?>