<?php
/**
 * Template Name: Contact Us
 */
?>
<?php get_header(); ?>


<div class="page-title grey">
    <div class="container">
        <div class="title-area text-center">
            <h2><?php the_title();?></h2>
            <div class="bread">
                <ol class="breadcrumb">
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li class="active">Get in touch</li>
                </ol>
            </div><!-- end bread -->
        </div><!-- /.pull-right -->
    </div>
</div><!-- end page-title -->

<div id="map"></div>

<section class="section white">
    <div class="container">
        <div class="general-title text-center">
            <h4>Contact Details</h4>
            <p class="lead"><?php the_field('contact_text'); ?></p>
            <hr>
        </div><!-- end general title -->

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="workinghours">
                    <ul>
                        <li>Weekdays <span><?php the_field('weekday_hours'); ?></span></li>
                        <li>Weekend <span><?php the_field('weekend_hours'); ?></span></li>
                        <li>Phone <span><a href="tel:<?php the_field('phone_number'); ?>" target="_blank"><?php the_field('phone_number'); ?></a></span></li>
                        <li>E-Mail <span><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></span></li>
                    </ul>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->


<?php get_footer(); ?>