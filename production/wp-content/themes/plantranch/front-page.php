<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php wp_head(); ?>

</head>
<body class="boxed comingsoon">  




    <div id="loader">
        <div class="loader-container">
            <img src="http://www.plantranch.ca/production/wp-content/uploads/2015/11/load.gif" alt="" class="loader-site spinner">
        </div>
    </div>


    <div id="wrapper">
        <div class="topbar clearfix">
            <div class="container">
                <div class="row-fluid">
                    <div class="col-md-6 text-left">
                        <div class="social">
                            <a href="https://www.facebook.com/Plant-Ranch-Inc-123560576385/" data-tooltip="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>              
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="https://www.linkedin.com/in/lucille-bouvier-2a451840" data-tooltip="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                        </div><!-- end social -->
                    </div><!-- end left -->
                    <div class="col-md-6 text-right">
                        <p>
                            <strong><i class="fa fa-phone"></i></strong> 306-525-1352 &nbsp;&nbsp;
                            <strong><i class="fa fa-envelope"></i></strong> <a href="mailto:info.plantranch.ca">info.plantranch.ca</a>
                        </p>
                    </div><!-- end left -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end topbar -->


    
        <section id="comingsoon" class="section">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="videobg text-center">
                            <img src="http://www.plantranch.ca/production/wp-content/uploads/2015/11/apple-touch-icon-120x120.png" alt="">
                            <h1>Coming Soon!!</h1>
                            <p class="lead">Site is under construction now,We will be with you as soon as possible. Please contact us directly at the information provided below.</p>
                        </div>
                    </div><!--end col -->
                </div><<!--end row -->
            </div><!-- end container -->
        </section><!-- end section -->
    </div><!-- end wrapper -->
<?php get_footer(); ?>