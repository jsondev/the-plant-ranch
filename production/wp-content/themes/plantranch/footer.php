<?php wp_footer(); ?>
<footer class="footer">
            <div class="container">
                <div class="row module-wrapper">
                    <div class="col-md-3 col-md-offset-2 col-sm-6">
                        <div class="widget">
                            <div class="widget-title">
                                <h4>Contact Us</h4>
                            </div><!-- end title -->
                            <ul class="site-links">




                                <li><i class="fa fa-link"></i><a href="www.plantranch.ca">www.plantranch.ca</a></li>
                                <li><i class="fa fa-envelope"></i><a href="mailto:info.plantranch.ca">info.plantranch.ca</a></li>
                                <li><i class="fa fa-phone"></i><a href="tel:1-306-525-1352" target="_blank">306-525-1352</a></li>
                                <li><i class="fa fa-home"></i><a href="https://www.google.com/maps/dir/Current+Location/5909%2013th%20Ave,%20Regina,%20SK%20S4W%201A9" target="_blank">Box 651 Regina, SK S4P 3A3</a></li>
                            </ul>
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-md-offset-2 col-sm-6">
                        <div class="widget">
                            <div class="widget-title">
                                <h4>Useful Links</h4>
                            </div>
                            <ul class="site-links">
                                    <?php if (have_rows('footer_useful_links_repeater', 'option')) { ?>
                                    <?php while ( have_rows('footer_useful_links_repeater', 'option') ) : the_row(); ?>
                                        <?php if( get_sub_field('useful_links_url' ,'option')) : ?> 
                                            <li>
                                                <a href="<?php the_sub_field('useful_links_url','option'); ?>">
                                                    <?php if( get_sub_field('useful_links_label' ,'option')) :  the_sub_field('useful_links_label','option'); else: ''; endif; ?>
                                                </a>
                                            </li> 
                                        <?php else: ''; endif; ?>
                                        <?php endwhile; ?>
                                        <?php } ?>
                            </ul>
                        </div><!-- end widget -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div>
        </footer>

        <section class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <p>© Copyright 2005 - The Plant Ranch - Regina, Saskatchewan CANADA, All rights reserved</p>
                    </div><!-- end col -->
                    <div class="col-md-6 text-right">
                        <ul class="list-inline">
                            <li><a href="#">Terms of Usage</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Sitemap</a></li>
                        </ul>
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
    </div><!-- end wrapper -->



    <!-- SLIDER REV -->
 
    <script>
    jQuery(document).ready(function() {
        jQuery('.tp-banner').show().revolution(
            {
            dottedOverlay:"none",
            delay:16000,
            startwidth:1170,
            startheight:600,
            hideThumbs:200,     
            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,  
            navigationType:"none",
            navigationArrows:"solo",
            navigationStyle:"preview2",  
            touchenabled:"on",
            onHoverStop:"on",
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,          
            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
            parallaxDisableOnMobile:"off",           
            keyboardNavigation:"off",   
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,
            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,  
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",
            spinner:"spinner4",  
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",  
            autoHeight:"off",           
            forceFullWidth:"off",                         
            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,            
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            fullScreenOffsetContainer: ""  
            }); 
        });
    </script>


    <script type="text/javascript">
        (function($) {
        "use strict";
            $('.selectpicker').selectpicker();
            $( "#datepicker" ).datepicker();
        })(jQuery);
    </script>



</body>
</html>