<?php

/**
 * Default css and
 */

function pp_public_css() {
	wp_register_style( 'pp-bootstrap', ASSETS_URL . '/css/flat-ui/bs/css/bs.css' );
	wp_enqueue_style( 'pp-flat-ui', ASSETS_URL . '/css/flat-ui/css/flat-ui.css', array( 'pp-bootstrap' ) );
	wp_enqueue_style( 'pp-social-button', ASSETS_URL . '/css/zocial/zocial.css' );
	wp_enqueue_style( 'ppcore', ASSETS_URL . '/css/ppcore.min.css' );
	wp_enqueue_style( 'font-awesome', ASSETS_URL . '/css/font-awesome/css/font-awesome.min.css' );
	wp_enqueue_style( 'pp-chosen', ASSETS_URL . '/chosen/chosen.min.css' );
}


function pp_admin_css() {
	wp_enqueue_style( 'pp-codemirror', ASSETS_URL . '/codemirror/codemirror.css' );
	wp_enqueue_style( 'pp-admin', ASSETS_URL . '/css/admin-style.css' );
	wp_enqueue_style( 'pp-chosen', ASSETS_URL . '/chosen/chosen.min.css' );
}


function pp_public_js() {
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'pp-zxcvbn', '/wp-includes/js/zxcvbn.min.js', array(), false, true );
	wp_enqueue_script( 'jquery-ui-datepicker' );
	wp_enqueue_script( 'password-strength-meter' );
	wp_enqueue_script( 'pp-bootstrap-filestyle', ASSETS_URL . '/js/bootstrap-filestyle.js', array( 'jquery' ) );
	wp_enqueue_script( 'pp-jcarousel', ASSETS_URL . '/js/jcarousel.js', array( 'jquery' ) );
	wp_enqueue_script( 'pp-sweetalert2', ASSETS_URL . '/js/sweetalert2.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'pp-del-avatar-script', ASSETS_URL . '/js/pp-del-avatar.js', array( 'jquery' ) );
	wp_localize_script( 'pp-del-avatar-script', 'pp_del_avatar_obj',
		array(
			'ajaxurl'   => admin_url( 'admin-ajax.php' ),
			'preloader' => ASSETS_URL . '/images/preload.gif',
			'nonce'     => wp_create_nonce( 'del-avatar-nonce' )
		)
	);
	wp_enqueue_script( 'pp-chosen', ASSETS_URL . '/chosen/chosen.jquery.min.js', array( 'jquery' ) );
}

function pp_admin_js() {
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-datepicker', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-ui-sortable' );

	wp_enqueue_script( 'pp-admin-scripts', ASSETS_URL . '/js/admin.js', array( 'jquery', 'jquery-ui-sortable' ) );
	wp_enqueue_script( 'pp-codemirror', ASSETS_URL . '/codemirror/codemirror.js' );
	wp_enqueue_script( 'pp-codemirror-css', ASSETS_URL . '/codemirror/css.js' );
	wp_enqueue_script( 'pp-chosen', ASSETS_URL . '/chosen/chosen.jquery.min.js', array( 'jquery' ) );
}


add_action( 'wp_enqueue_scripts', 'pp_public_css' );
add_action( 'admin_enqueue_scripts', 'pp_admin_css' );
add_action( 'wp_enqueue_scripts', 'pp_public_js' );
add_action( 'admin_enqueue_scripts', 'pp_admin_js' );
