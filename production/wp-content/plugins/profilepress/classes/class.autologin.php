<?php

/**
 * Class to automatic log user in after registration
 */
class PP_Auto_Login_After_Registration {

	/**
	 * Initialize class
	 *
	 * @param int $user_id
	 * @param string $login_id
	 * @param string $redirect
	 */
	public static function initialize( $user_id, $login_id = '', $redirect = '' ) {

		if ( ! pp_user_id_exist( $user_id ) ) {
			return;
		}

		do_action( 'pp_before_auto_login', $login_id, $user_id );

		$set_login_cookie = apply_filters( 'pp_auto_login_set_cookie', true );

		if ( $set_login_cookie ) {
			wp_set_auth_cookie( $user_id );
			wp_set_current_user( $user_id );
		}

		do_action( 'pp_before_auto_login_redirect', $login_id, $user_id );

		$login_redirect = ! empty( $redirect ) ? $redirect : pp_login_redirect();

		/** Setup a custom location for "auto login after registration" */
		$login_redirection = apply_filters( 'pp_auto_login_redirection', $login_redirect, $login_id, $user_id );

		wp_redirect( $login_redirection );
		exit;
	}

}