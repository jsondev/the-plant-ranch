<?php

class ProfilePress_Dir {

	public static function load_files() {

		require CLASSES . '/class.plugin-update.php';
		require CLASSES . '/global-functions.php';
		require CLASSES . '/tgm-dependencies.php';
		require CLASSES . '/load-shortcake.php';
		require CLASSES . '/action-links.php';

		require PROFILEPRESS_ROOT . 'help-tab/change-help-tab-text.php';

		require CLASSES . '/profilepress-sql.php';
		require CLASSES . '/class.parse-settings-args.php';
		require CLASSES . '/profilepress-avatar.php';
		require CLASSES . '/profile-url-rewrite.php';
		require CLASSES . '/alter-password-reset-title-message.php';
		require CLASSES . '/class.alter-default-links.php';
		require CLASSES . '/register-default-component.php';
		require CLASSES . '/class.upload-avatar.php';
		require CLASSES . '/class.files-uploader.php';
		require CLASSES . '/class.edit-user-profile.php';
		require CLASSES . '/buddypress-bbpress.php';
		require CLASSES . '/admin-notices.php';

		require CLASSES . '/ajax-handler.php';
		require CLASSES . '/class.passwordless-login.php';
		require CLASSES . '/class.autologin.php';
		require CLASSES . '/class.login-form-auth.php';
		require CLASSES . '/moderation-notification.php';

		require SOCIAL_LOGIN . '/class.social-login.php';
		require SOCIAL_LOGIN . '/connectors.php';
		require SOCIAL_LOGIN . '/logout.php';

		require RECAPTCHA . '/class.recaptcha.php';
		require RECAPTCHA . '/registration-form.php';
		require RECAPTCHA . '/login-form.php';
		require RECAPTCHA . '/password-reset.php';
		require RECAPTCHA . '/edit-user-profile.php';

		require CLASSES . '/global-shortcodes/global-shortcodes.php';
		require CLASSES . '/global-shortcodes/social-buttons.php';

		require WIDGETS . '/tab-log-reg-pass.php';
		require WIDGETS . '/login.php';
	    require WIDGETS . '/registration.php';
	    require WIDGETS . '/password-reset.php';
	    require WIDGETS . '/edit-profile.php';
	    require WIDGETS . '/melange.php';

		require VIEWS . '/admin-footer.php';
		require VIEWS . '/general-settings.php';
		require VIEWS . '/profile-contact-info/contact-info-settings-page.php';
		require VIEWS . '/custom-profile-fields/custom-profile-field-settings.php';
		require VIEWS . '/login-form-builder/parent-login-shortcode-parser.php';
		require VIEWS . '/registration-form-builder/parent-registration-shortcode-parser.php';
		require VIEWS . '/password-reset-builder/parent-password-reset-shortcode-parser.php';
		require VIEWS . '/edit-user-profile/parent-edit-profile-shortcode-parser.php';
		require VIEWS . '/front-end-user-profile/parent-front-end-profile-shortcode-parser.php';
		require VIEWS . '/melange/parent-melange-shortcode-parser.php';
		require VIEWS . '/social-login/settings-page.php';
		require VIEWS . '/extras/extras-settings-page.php';
		require VIEWS . '/theme-install/settings-page.php';
		require VIEWS . '/license/settings-page.php';
		require VIEWS . '/revision/settings-page.php';
	}
}