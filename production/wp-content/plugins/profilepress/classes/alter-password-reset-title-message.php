<?php

/**
 * Alter the default password reset title and message
 */
class ProfilePress_Alter_Password_Reset {

	/** @type array plugin general settings */
	private $db_settings;

	/** @type  object singleton instance */
	static private $instance;

	/** Constructor */
	public function __construct() {

		$this->db_settings = get_option( 'pp_settings_data' );

		add_action( 'retrieve_password_title', array( $this, 'title' ) );

		add_action( 'retrieve_password_message', array( $this, 'message' ), 10, 2 );
	}


	/**
	 * Change the password reset title.
	 *
	 * @param $title string filter title
	 *
	 * @return string
	 */
	public function title( $title ) {
		$title = $this->db_settings['password_reset_subject'];

		return $title;
	}

	/** @return mixed Raw password reset message from plugin DB settings */
	public function raw_reset_message() {
		return $this->db_settings['password_reset_message'];
	}


	/**
	 * Return the formatted password reset message.
	 *
	 * @param string $user_login username
	 * @param string $key activation key
	 *
	 * @return string
	 */
	public function formatted_message( $user_login, $key ) {

		$raw_message = $this->raw_reset_message();

		$search = array(
			'{{username}}',
			'{{password_reset_link}}'
		);

		$replace = array(
			$user_login,
			pp_get_do_password_reset_url( $user_login, $key )
		);

		$formatted_message = str_replace( $search, $replace, $raw_message );

		return htmlspecialchars_decode( $formatted_message );
	}


	/**
	 * Callback function for filter
	 *
	 * @param $message string default reset message supply by password reset form
	 * @param $key string activation key supplied by filter
	 *
	 * @return string formatted message for use by the password reset form
	 */
	public function message( $message, $key ) {

		// perform the rituals below to get the username/email entered into the password reset form
		if ( strpos( $_POST['user_login'], '@' ) ) {
			$user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
		}
		else {
			$user_data = get_user_by( 'login', trim( $_POST['user_login'] ) );
		}

		// Alas! we got the damn user_login
		$user_login = $user_data->user_login;

		$message = $this->formatted_message( $user_login, $key );

		return $message;

	}


	/**
	 * Singleton instance.
	 *
	 * @return object|ProfilePress_Alter_Password_Reset
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;

	}
}

ProfilePress_Alter_Password_Reset::get_instance();