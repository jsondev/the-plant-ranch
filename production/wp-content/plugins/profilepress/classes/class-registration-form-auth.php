<?php

// include the auto-login class
require_once CLASSES . '/class.autologin.php';

/** @todo refactor this class to be modular and easy to @see moderation-notification.php */
class ProfilePress_Registration_Auth {

	static protected $registration_form_status;

	// recaptcha db settings
	static protected $recaptcha_db_settings;

	/**
	 * Called to validate registration form field
	 *
	 * @param string $id
	 * @param string $redirect
	 * @param bool $is_melange
	 *
	 * @return string
	 */
	public static function validate_registration_form( $id = '', $redirect = '', $is_melange = false ) {
		// if registration form have been submitted process it

		// filter to change registration submit button name to avoid validation for forms on same page
		$submit_name = apply_filters( 'pp_registration_submit_name', 'reg_submit', $id );
		if ( isset( $_POST[ $submit_name ] ) ) {
			$register_the_user = ProfilePress_Registration_Auth::register_new_user( $_POST, $id, $_FILES, $redirect, $is_melange );
		}

		// display form generated messages
		if ( ! empty( $register_the_user ) ) {
			$registration_errors = html_entity_decode( $register_the_user );
		}
		else {
			$registration_errors = '';
		}

		return $registration_errors;
	}

	/**
	 * Wrapper function for call to the welcome email class
	 *
	 * @param int $form_id
	 * @param string $email
	 * @param string $username
	 * @param string $password
	 * @param string $first_name
	 * @param string $last_name
	 */
	static function send_welcome_email( $form_id, $email, $username, $password, $first_name, $last_name ) {

		$db_settings_data = pp_db_data();

		// check if sending of welcome mail after registration is activated
		$send_welcome_mail_status = apply_filters( 'pp_activate_send_welcome_email', @$db_settings_data['set_welcome_message_after_reg'] );

		if ( $send_welcome_mail_status == 'on' ) {

			do_action( 'pp_before_send_welcome_mail', $form_id, $email, $username, $password, $first_name, $last_name );

			// send welcome email
			new Send_Email_After_Registration( $form_id, $email, $username, $password, $first_name, $last_name );

			do_action( 'pp_after_send_welcome_mail', $form_id, $email, $username, $password, $first_name, $last_name );
		}
	}


	/**
	 * Wrapper function for call to the automatic login after reg function
	 *
	 * @param int $user_id
	 * @param int $form_id
	 * @param string $redirect redirect url after registration
	 */
	static function auto_login_after_reg( $user_id, $form_id, $redirect ) {

		if ( ! empty( $redirect ) ) {
			PP_Auto_Login_After_Registration::initialize( $user_id, $form_id, $redirect );
		}
		else {

			$db_settings_data = pp_db_data();

			$activate_auto_login = ! empty( $db_settings_data['set_auto_login_after_reg'] ) ? $db_settings_data['set_auto_login_after_reg'] : '';

			$auto_login_option = apply_filters( 'pp_activate_auto_login_after_signup', $activate_auto_login, $form_id );

			if ( $auto_login_option == 'on' ) {
				PP_Auto_Login_After_Registration::initialize( $user_id, $form_id );
			}
		}
	}


	/**
	 * Register new users
	 *
	 * @param $post array $_POST data
	 * @param $form_id int Registration builder ID
	 *
	 * @return string
	 */
	public static function register_new_user( $post, $form_id, $files = '', $redirect = '', $is_melange ) {

		// create an array of acceptable userdata for use by wp_insert_user
		$valid_userdata = array(
			'reg_username',
			'reg_password',
			'reg_password2',
			'reg_email',
			'reg_website',
			'reg_nickname',
			'reg_display_name',
			'reg_first_name',
			'reg_last_name',
			'reg_bio'
		);

		// get the data for userdata
		$segregated_userdata = array();

		// loop over the $_POST data and create an array of the wp_insert_user userdata
		foreach ( $post as $key => $value ) {
			if ( $key == 'reg_submit' ) {
				continue;
			}

			if ( in_array( $key, $valid_userdata ) ) {
				$segregated_userdata[ $key ] = esc_attr( $value );
			}
		}


		// get the data for use by update_meta
		$custom_usermeta = array();

		// loop over the $_POST data and create an array of the invalid userdata/ custom usermeta
		foreach ( $post as $key => $value ) {
			if ( $key == 'reg_submit' ) {
				continue;
			}

			if ( ! in_array( $key, $valid_userdata ) ) {
				$custom_usermeta[ $key ] = is_array($value) ? array_map('sanitize_text_field', $value) : sanitize_text_field($value);
			}
		}

		// get convert the form post data to userdata for use by wp_insert_users
		$username     = isset( $segregated_userdata['reg_username'] ) ? $segregated_userdata['reg_username'] : '';
		$password     = isset( $segregated_userdata['reg_password'] ) ? $segregated_userdata['reg_password'] : '';
		$password2    = isset( $segregated_userdata['reg_password2'] ) ? $segregated_userdata['reg_password2'] : null;
		$email        = isset( $segregated_userdata['reg_email'] ) ? $segregated_userdata['reg_email'] : '';
		$website      = isset( $segregated_userdata['reg_website'] ) ? $segregated_userdata['reg_website'] : '';
		$nickname     = isset( $segregated_userdata['reg_nickname'] ) ? $segregated_userdata['reg_nickname'] : '';
		$display_name = isset( $segregated_userdata['reg_display_name'] ) ? $segregated_userdata['reg_display_name'] : '';
		$first_name   = isset( $segregated_userdata['reg_first_name'] ) ? $segregated_userdata['reg_first_name'] : '';
		$last_name    = isset( $segregated_userdata['reg_last_name'] ) ? $segregated_userdata['reg_last_name'] : '';
		$bio          = isset( $segregated_userdata['reg_bio'] ) ? $segregated_userdata['reg_bio'] : '';


		// real uer data
		$real_userdata = array(
			'user_login'   => $username,
			'user_pass'    => $password,
			'user_email'   => $email,
			'user_url'     => $website,
			'nickname'     => $nickname,
			'display_name' => $display_name,
			'first_name'   => $first_name,
			'last_name'    => $last_name,
			'description'  => $bio
		);

		$builder_role = PROFILEPRESS_sql::get_registration_user_role( $form_id );
		if ( ! empty( $builder_role ) ) {
			$real_userdata['role'] = $builder_role;
		}

		// filter for the css class of the error message
		$reg_status_css_class = apply_filters( 'pp_registration_error_css_class', 'profilepress-reg-status', $form_id );


		/* start filter Hook */
		$reg_errors = new WP_Error();

		if ( ! is_email( $real_userdata['user_email'] ) ) {
			$reg_errors->add( 'invalid_email', __( 'Email address is not valid', 'profilepress' ) );
		}

		if ( isset( $password2 ) && ( $password != $password2 ) ) {
			$reg_errors->add( 'password_mismatch', __( 'Passwords do not match', 'profilepress' ) );
		}

		if ( isset( $post['pp_enforce_password_meter'] ) && ( $post['pp_enforce_password_meter'] != '1' ) ) {
			$reg_errors->add( 'password_weak', __( 'Password is not strong', 'profilepress' ) );
		}


		// call validate reg from function
		$reg_form_errors = apply_filters( 'pp_registration_validation', $reg_errors, $form_id );

		if ( is_wp_error( $reg_form_errors ) && $reg_form_errors->get_error_code() != '' ) {
			return '<div class="' . $reg_status_css_class . '">' . $reg_form_errors->get_error_message() . '</div>';
		}
		/* End Filter Hook */

		// --------START ---------   validation for file upload ----------------------//
		$uploads       = PP_File_Uploader::init();
		$upload_errors = '';
		foreach ( $uploads as $field_key => $uploaded_filename_or_wp_error ) {
			if ( is_wp_error( $uploads[ $field_key ] ) ) {
				$upload_errors .= $uploads[ $field_key ]->get_error_message() . '<br/>';
			}
		}
		if ( ! empty( $upload_errors ) ) {
			return "<div class='$reg_status_css_class'>$upload_errors</div>";
		}
		// --------END ---------   validation for file upload ----------------------//


		//merge real data(for use by wp_insert_user()) and custom profile fields data
		$user_data = array_merge( $real_userdata, $custom_usermeta );

		/* Start Action Hook */
		do_action( 'pp_before_registration', $form_id, $user_data );
		/* End Action Hook */

		// proceed to registration using wp_insert_user method which return the new user id
		$user_id = wp_insert_user( $real_userdata );

		// if moderation is active, set new registered users as pending
		if ( self::moderation_is_active() ) {
			if ( is_null( get_role( 'pending_users' ) ) ) {
				add_role( 'pending_users', 'Pending' );
			}

			// make registered user pending.
			ProfilePress_User_Moderation_Admin::make_user_pending( $user_id );
		}


		$new_user_notification = apply_filters( 'pp_new_user_notification', 'enable' );

		if ( is_int( $user_id ) && 'enable' == $new_user_notification ) {
			wp_new_user_notification( $user_id, null, 'admin' );
		}

		// register custom profile field
		if ( ! is_wp_error( $user_id ) ) {

			// if user selected a photo for upload, process it.
			if ( isset( $files['reg_avatar']['name'] ) && ! empty( $files['reg_avatar']['name'] ) ) {
				$upload_avatar = PP_User_Avatar_Upload::process( $files['reg_avatar'] );

				// update custom profile field
				$custom_usermeta['pp_profile_avatar'] = $upload_avatar;

				// if we get to this point, it means the files pass validation defined above.
				// array of files uploaded. Array key is the "custom field key" and the filename as the array value.
				$custom_usermeta['pp_uploaded_files'] = $uploads;
			}

			// if @$user_id is no WP_Error, add the extra user profile field
			if ( is_array( $custom_usermeta ) ) {

				foreach ( $custom_usermeta as $key => $value ) {
					update_user_meta( $user_id, $key, $value );
				}
			}

			// if user moderation is active, send pending notification.
			if ( self::moderation_is_active() ) {
				PP_User_Moderation_Notification::pending( $user_id );
				PP_User_Moderation_Notification::pending_admin_notification( $user_id );
			}

			// send welcome message
			self::send_welcome_email( $form_id, $email, $username, $password, $first_name, $last_name );


			/** Start Action Hook
			 *
			 * @param int $form_id registration form ID
			 * @param mixed $user_data array of user info.
			 * @param int $user_id registered user ID
			 */
			do_action( 'pp_after_registration', $form_id, $user_data, $user_id );
			/* End Action Hook */

			/**
			 * call auto-login and send email after registration
			 *
			 * @param int $user_id registered user ID
			 * @param int $form_id registration form ID
			 * @param string $redirect redirect url after login
			 */
			self::auto_login_after_reg( $user_id, $form_id, $redirect );


			// get the "registration successful message" for the registration page
			if ( $is_melange ) {
				$message_on_successful_registration = PROFILEPRESS_sql::get_db_success_melange( $form_id, 'registration_msg' );
			}
			else {
				$message_on_successful_registration = PROFILEPRESS_sql::get_db_success_registration( $form_id );
			}

			return ! empty( $message_on_successful_registration ) ? $message_on_successful_registration : '<div class="profilepress-reg-status">' . __( 'Registration successful.', 'profilepress' ) . '</div>';
		}
		else {
			return '<div class="' . $reg_status_css_class . '">' . $user_id->get_error_message() . '</div>';
		}

	}


	/**
	 * Check if the user moderation module is active.
	 * @return bool
	 */
	public static function moderation_is_active() {

		$extra_moderation_data = get_option( 'pp_extra_moderation' );

		if ( isset( $extra_moderation_data['activate_moderation'] ) && $extra_moderation_data['activate_moderation'] == 'active' ) {
			return true;
		}
		else {
			return false;
		}
	}
}