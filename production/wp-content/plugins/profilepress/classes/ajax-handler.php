<?php

//builder preview Ajax handler
add_action( 'wp_ajax_pp-builder-preview', 'pp_builder_preview_handler' );

function pp_builder_preview_handler() {

	// iframe preview url content
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'pp-builder-preview' ) {
		include VIEWS . '/live-preview/builder-preview.php';
	} // if ajax post request is received return the parsed shortcode
	elseif ( isset( $_POST['builder_structure'] ) && ! empty( $_POST['builder_structure'] ) ) {
		Front_End_Profile_Builder_Shortcode_Parser::get_instance();
		echo do_shortcode( stripslashes( $_POST['builder_structure'] ) );
	}

	// IMPORTANT: don't forget to "exit"
	wp_die();

}

//builder preview Ajax handler
add_action( 'wp_ajax_pp_del_avatar', 'pp_ajax_delete_avatar' );

function pp_ajax_delete_avatar() {
	// check to see if the submitted nonce matches with the generated nonce we created earlier
	if ( ! wp_verify_nonce( $_POST['nonce'], 'del-avatar-nonce' ) ) {

		$response = json_encode( array( 'error' => 'nonce_failed' ) );

	} else {
		ProfilePress_Edit_Profile::remove_avatar_core();

		// generate the response
		$response = json_encode( array( 'success' => true, 'default' => pp_get_avatar_url('', '300') ) );
	}

	// response output
	header( "Content-Type: application/json" );
	echo $response;

	// IMPORTANT: don't forget to "exit"
	wp_die();

}


add_action( 'wp_ajax_pp_profile_fields_sortable', 'pp_profile_fields_sortable_func' );

function pp_profile_fields_sortable_func() {
	global $wpdb;

	$posted_data       = array_map( 'absint', $_POST['data'] );
	$profile_field_ids = PROFILEPRESS_sql::get_profile_field_ids();
	$table_name        = "{$wpdb->base_prefix}pp_profile_fields";

	/* Alter the IDs of the profile fields in DB incrementally starting from the last ID number of the record. */

	// set the index to the last profile field ID
	$index = array_pop( $profile_field_ids ) + 1;

	foreach ( $posted_data as $id ) {

		$wpdb->update(
			$table_name,
			array(
				'id' => $index
			),
			array( 'id' => $id ),
			array(
				'%d'
			),
			array( '%d' )
		);

		$index ++;
	}


	/* Reorder the profile fields ID starting from 1 incrementally. */

	$index_2 = 1;

	// fetch the profile fields again
	$profile_field_ids_2 = PROFILEPRESS_sql::get_profile_field_ids();

	foreach ( $profile_field_ids_2 as $id ) {
		$wpdb->update(
			$table_name,
			array(
				'id' => $index_2
			),
			array( 'id' => $id ),
			array(
				'%d'
			),
			array( '%d' )
		);

		$index_2 ++;
	}

	wp_die();
}

add_action( 'wp_ajax_pp_contact_info_sortable', 'pp_pp_contact_info_sortable_func' );

function pp_pp_contact_info_sortable_func() {

	$posted_data = array_map( 'esc_attr', $_POST['data'] );
	$db_data     = get_option( 'pp_contact_info', array() );

	$newArray = array();

	foreach ( $posted_data as $key ) {
		$newArray[ $key ] = $db_data[ $key ];
	}

	update_option( 'pp_contact_info', $newArray );

	wp_die();
}