<?php

/** Handles editing profile of logged in user */
class ProfilePress_Edit_Profile {

	/**
	 * Called to validate the password reset form
	 *
	 * @param int $id buider ID
	 * @param bool $is_melange is the validation being called from a melange?
	 *
	 * @return string error or success message
	 */
	public static function validate_form( $id = null, $is_melange = false ) {
		// make $post global so the page/post ID can be retrieve for comparison below
		global $post;

		$response = self::process_func( $_POST, $_FILES, $id, $is_melange );

		// check if the page being viewed contains the "edit profile" shortcode. if true, redirect to login page
		if ( has_shortcode( $post->post_content, 'profilepress-edit-profile' ) ) {
			if ( ! is_user_logged_in() ) {
				wp_redirect( wp_login_url() );
				exit;
			}
		}

		return $response;
	}

	/**
	 * @param $post_data array $_POST data
	 * @param $file_data array $_FILES submitted file via form
	 * @param $form_id int builder ID
	 *
	 * @return string the edit profile response be it error or success message
	 */
	public static function process_func( $post_data, $file_data, $form_id, $is_melange ) {

		// if edit-profile form have been submitted process it

		// filter to change edit-profile form submit button name to avoid validation for forms on same page
		$submit_name = apply_filters( 'pp_edit_profile_submit_name', 'eup_submit', $form_id );

		if ( isset( $post_data[ $submit_name ] ) ) {

			$edit_profile_response = self::update_user_profile( $post_data, $file_data, $form_id );
		} // if registration form have been submitted process it
		elseif ( isset( $post_data['eup_remove_avatar'] ) && $post_data['eup_remove_avatar'] == 'removed' ) {
			self::remove_user_avatar();
		}


		// get the success message on profile edit
		if ( $is_melange ) {
			$message_on_successful_update = PROFILEPRESS_sql::get_db_success_melange( $form_id, 'edit_profile_msg' );
		}
		else {
			$message_on_successful_update = PROFILEPRESS_sql::get_db_success_edit_profile( $form_id );
		}

		$message_on_successful_update = isset( $message_on_successful_update ) ? $message_on_successful_update : '<h5>Profile Successfully Edited</h5>';


		// display form generated messages else the success message
		if ( ! empty( $edit_profile_response ) ) {
			return '<div class="profilepress-edit-profile-status">' . $edit_profile_response . '</div>';
		}
		elseif ( isset( $_GET['edit'] ) && ( $_GET['edit'] ) ) {
			return html_entity_decode( $message_on_successful_update );
		}

	}


	/** Get the current user id */
	public static function get_current_user_id() {
		return get_current_user_id();
	}


	/**
	 * Update user profile
	 *
	 * @param $post array global for $_POST data
	 * @file $file array  global $_FILES for file upload
	 *
	 * @return WP_Error|void
	 */
	public static function update_user_profile( $post, $files, $form_id ) {


		/* Validate and add custom validation to edit profile */
		$validation_errors = apply_filters( 'pp_edit_profile_validation', '' );

		if ( is_wp_error( $validation_errors ) ) {
			return $validation_errors->get_error_message();
		} /* End of filter */

		else {
			// create an array of acceptable userdata for use by wp_update_user
			$valid_userdata = array(
				'eup_username',
				'eup_password',
				'eup_email',
				'eup_website',
				'eup_nickname',
				'eup_display_name',
				'eup_first_name',
				'eup_last_name',
				'eup_bio'
			);


			// get the escaped data for userdata
			$escaped_post_data = self::escaped_post_data( $post );


			// get the data for use by update_user_meta
			$custom_usermeta = self::custom_usermeta_data( $escaped_post_data, $valid_userdata );

			// convert the form post data to userdata for use by wp_update_users
			$real_userdata = array();

			$real_userdata['ID'] = self::get_current_user_id();

			if ( isset( $post['eup_password'] ) ) {
				$real_userdata['user_pass'] = $post['eup_password'];
			}

			if ( isset( $post['eup_email'] ) ) {
				$real_userdata['user_email'] = $post['eup_email'];
			}

			if ( isset( $post['eup_website'] ) ) {
				$real_userdata['user_url'] = $post['eup_website'];
			}

			if ( isset( $post['eup_nickname'] ) ) {
				$real_userdata['nickname'] = $post['eup_nickname'];
			}

			if ( isset( $post['eup_display_name'] ) ) {
				$real_userdata['display_name'] = $post['eup_display_name'];
			}

			if ( isset( $post['eup_first_name'] ) ) {
				$real_userdata['first_name'] = $post['eup_first_name'];
			}

			if ( isset( $post['eup_last_name'] ) ) {
				$real_userdata['last_name'] = $post['eup_last_name'];
			}

			if ( isset( $post['eup_bio'] ) ) {
				$real_userdata['description'] = $post['eup_bio'];
			}

			// merge real data(for use by wp_insert_user()) and custom profile fields data
			$user_data = array_merge( $real_userdata, $custom_usermeta );

			/**
			 * Fires before profile is updated
			 *
			 * @param $user_data array user_data of user being updated
			 * @param $form_id int builder ID
			 */
			do_action( 'pp_before_profile_update', $user_data, $form_id );


			if ( isset( $files['eup_avatar']['name'] ) && ! empty( $files['eup_avatar']['name'] ) ) {
				$upload_avatar = PP_User_Avatar_Upload::process( $files['eup_avatar'] );

				// update custom profile field
				$custom_usermeta['pp_profile_avatar'] = $upload_avatar;

				if ( is_wp_error( $upload_avatar ) ) {
					return $upload_avatar->get_error_message();
				}

			}


			// update file uploads
			$uploads       = PP_File_Uploader::init();
			$upload_errors = '';
			foreach ( $uploads as $field_key => $uploaded_filename_or_wp_error ) {
				if ( is_wp_error( $uploads[ $field_key ] ) ) {
					$upload_errors .= $uploads[ $field_key ]->get_error_message() . '<br/>';
				}
			}

			if ( empty( $upload_errors ) ) {
				// we get the old array of stored file for the user
				$old = get_user_meta( self::get_current_user_id(), 'pp_uploaded_files', true );
				$old = ! empty( $old ) ? $old : array();

				// we loop through the arra of newly uploaded files and remove any file (unsetting the file array key)
				// that isn't be updated i.e if the field is left empty, unsetting it prevent update_user_meta
				// fom overriding it.
				// we then merge the old and new uploads before saving the data to user meta table.
				foreach ( $uploads as $key => $value ) {
					if ( is_null( $value ) || empty( $value ) ) {
						unset( $uploads[ $key ] );
					}
				}

				update_user_meta( self::get_current_user_id(), 'pp_uploaded_files', array_merge( $old, $uploads ) );
			}
			else {
				return $upload_errors;
			}


			if ( is_array( $custom_usermeta ) ) {

				// loop over the custom user_meta and update data to DB
				foreach ( $custom_usermeta as $key => $value ) {
					update_user_meta(
						self::get_current_user_id(),
						$key,
						$value
					);
				}
			}

			// merge real data(for use by wp_insert_user()) and custom profile fields data
			$user_data = array_merge( $real_userdata, $custom_usermeta );

			// proceed to registration using wp_insert_user method which return the new user id
			$update_user = wp_update_user( $real_userdata );

			if ( ! is_wp_error( $update_user ) ) {

				/** Fires after profile is updated
				 *
				 * @param array $user_data
				 */
				do_action( 'pp_after_profile_update', $user_data );

				// ADD QUERY ARG WITH STATUS
				$redirect = apply_filters( 'pp_redirect_after_profile_edit', esc_url_raw( add_query_arg( 'edit', 'true' ) ) );
				wp_redirect( $redirect );
				exit;
			}
			elseif ( is_wp_error( $update_user ) ) {
				return $update_user->get_error_message();
			}
			else {
				return __( 'Something unexpected happened. Please try again', 'profilepress' );
			}
		}

	}


	/**
	 * Escaped the POST data
	 *
	 * @param $post_data array raw post data
	 *
	 * @return array
	 */
	public static function escaped_post_data( $post_data ) {

		// get the escaped data for userdata
		$escaped_post_data = array();

		// loop over the $_POST data and create an array of the wp_insert_user userdata
		foreach ( $post_data as $key => $value ) {
			if ( $key == 'eup_submit' ) {
				continue;
			}

			if ( is_array( $value ) ) {
				$escaped_post_data[ $key ] = array_map( 'sanitize_text_field', $value );
			}
			else {
				$escaped_post_data[ $key ] = sanitize_text_field( $value );
			}
		}

		return $escaped_post_data;

	}


	/**
	 * @param $post_data array escaped $_POST Data @see self::escaped_post_data
	 *
	 * @param $valid_userdata array userdata valid for wp_update_user
	 *
	 * @return array
	 */
	public static function custom_usermeta_data( $post_data, $valid_userdata ) {

		// get the data for use by update_user_meta
		$custom_usermeta = array();

		// loop over the $_POST data and create an array of the invalid userdata/ custom usermeta
		foreach ( $post_data as $key => $value ) {
			if ( $key == 'eup_submit' ) {
				continue;
			}

			if ( ! in_array( $key, $valid_userdata ) ) {
				$custom_usermeta[ $key ] = $value;
			}
		}

		return $custom_usermeta;
	}


	/**
	 * Remove user avatar and redirect. Triggered when JS is disabled.
	 */
	public static function remove_user_avatar() {

		self::remove_avatar_core();

		// ADD QUERY ARG WITH STATUS
		wp_redirect( esc_url( add_query_arg( 'edit', 'true' ) ) );
		exit;
	}


	/**
	 * Core function that removes/delete the user's avatar
	 */
	public static function remove_avatar_core() {
		$avatar_upload_dir = AVATAR_UPLOAD_DIR;

		$avatar_slug = get_user_meta( self::get_current_user_id(), 'pp_profile_avatar', true );

		do_action( 'pp_before_avatar_removal', $avatar_slug );

		// delete the file profile image from server
		unlink( $avatar_upload_dir . $avatar_slug );

		// delete the record from DB
		delete_user_meta( self::get_current_user_id(), 'pp_profile_avatar' );

		do_action( 'pp_after_avatar_removal' );
	}

}