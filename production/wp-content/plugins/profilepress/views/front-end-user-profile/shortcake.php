<?php

add_action( 'init', 'pp_frontend_profile_shortcode_shortcake' );

function pp_frontend_profile_shortcode_shortcake() {
	if ( !function_exists( 'shortcode_ui_register_for_shortcode' ) ) {
		return;
	}

	shortcode_ui_register_for_shortcode(
		'profile-username',
		array(
			'label'         => 'Front-end Profile: Username',
			'listItemImage' => 'dashicons-admin-users',
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-email',
		array(
			'label'         => 'Front-end Profile: Email Address',
			'listItemImage' => 'dashicons-email',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-website',
		array(
			'label'         => 'Front-end Profile: Website',
			'listItemImage' => 'dashicons-admin-links',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-nickname',
		array(
			'label'         => 'Front-end Profile: Nickname',
			'listItemImage' => 'dashicons-admin-users',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-display-name',
		array(
			'label'         => 'Front-end Profile: Display Name',
			'listItemImage' => 'dashicons-admin-users',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-first-name',
		array(
			'label'         => 'Front-end Profile: First Name',
			'listItemImage' => 'dashicons-admin-users',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-last-name',
		array(
			'label'         => 'Front-end Profile: Last name',
			'listItemImage' => 'dashicons-admin-users',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-bio',
		array(
			'label'         => 'Front-end Profile: Biography',
			'listItemImage' => 'dashicons-info',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-avatar-url',
		array(
			'label'         => 'Front-end Profile: Avatar Image URL',
			'listItemImage' => 'dashicons-admin-links',
			
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-cpf',
		array(
			'label'         => 'Front-end Profile: Custom Field Data',
			'listItemImage' => 'dashicons-editor-code',
			'attrs'         => array(
				array(
					'label'       => 'Field Key',
					'attr'        => 'key',
					'type'        => 'text',
					'description' => 'Custom profile field\'s key.',
				)
			)
		)
	);

	shortcode_ui_register_for_shortcode(
		'profile-file',
		array(
			'label'         => 'Front-end Profile: Uploaded File',
			'listItemImage' => 'dashicons-upload',
			'attrs'         => array(
				array(
					'label'       => 'File Key',
					'attr'        => 'key',
					'type'        => 'text',
					'description' => 'Custom profile field\'s key for the file.',
				)
			)
		)
	);

	shortcode_ui_register_for_shortcode(
		'post-count',
		array(
			'label'         => 'Front-end Profile: Post Count',
			'listItemImage' => 'dashicons-admin-post'
		)
	);

	shortcode_ui_register_for_shortcode(
		'comment-count',
		array(
			'label'         => 'Front-end Profile: Comment Count',
			'listItemImage' => 'dashicons-admin-comments'
		)
	);
}