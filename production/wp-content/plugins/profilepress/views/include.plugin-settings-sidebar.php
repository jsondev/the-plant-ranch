<div id="postbox-container-1" class="postbox-container">

	<div class="meta-box-sortables" style="text-align: center; margin: auto">
		<div class="postbox">
			<div class="handlediv" title="Click to toggle"><br></div>
			<h3 class="hndle ui-sortable-handle"><span><?php _e( 'Support / Documentation', 'profilepress' ); ?></span>
			</h3>

			<div class="inside">
				<p><?php printf( __( 'For support, visit the %s', 'profilepress' ), '<strong><a href="http://support.profilepress.net" target="blank">' . __( 'forum', 'profilepress' ) . '</a>.</strong>' ); ?></p>

				<p><?php printf( __( 'Visit the %s for guidance', 'profilepress' ), '<strong><a href="http://docs.profilepress.net" target="blank">' . __( 'Documentation', 'profilepress' ) . '</a></strong>' ); ?></p>

			</div>
		</div>

		<div class="postbox">
			<div class="handlediv" title="Click to toggle"><br></div>
			<h3 class="hndle ui-sortable-handle"><span><?php _e( 'ProfilePress Extensions', 'profilepress' ); ?></span>
			</h3>

			<div class="inside">
				<ul>
					<li>
						<a href="http://profilepress.net/downloads/mailchimp/" target="_blank">MailChimp</a>: <?php _e( 'automatically subscribe new users to your MailChimp list.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/email-confirmation/" target="_blank"><?php _e( 'Email Confirmation', 'profilepress' ); ?></a>: <?php _e( ' ensures new registered users confirm their email addresses before they can login.', 'profilepress' ); ?>
					</li>
				</ul>
				<div><a href="http://profilepress.net/extensions/" target="_blank">
						<button class="button-primary" type="button"><?php _e( 'Shop Now', 'profilepress' ); ?></button>
					</a></div>
			</div>
		</div>

		<div class="postbox">
			<div class="handlediv" title="Click to toggle"><br></div>
			<h3 class="hndle ui-sortable-handle"><span><?php _e( 'ProfilePress Themes', 'profilepress' ); ?></span>
			</h3>

			<div class="inside">
				<ul>
					<li>
						<a href="http://profilepress.net/downloads/pinnacle-account-form/" target="_blank">Pinnacle</a>: <?php _e( 'a smooth and clean custom login, registration and password reset form theme with social login.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/montserrat-account-form/" target="_blank">Montserrat</a>: <?php _e( 'a beautiful one-page form that combines a login, registration, password reset and social login together thus eliminating the need of creating separate forms.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/stride-wordpress-multi-step-registration-form/" target="_blank">Stride</a>: <?php _e( 'an elegant, customizable WordPress multi-step registration form.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/bash-account-form/" target="_blank">Bash</a>: <?php _e( 'a simple, pretty, responsive one-page account form with social login.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/tag/parallel/" target="_blank">Parallel</a>: <?php _e( 'a Horizontal, responsive and aesthetic login, registration & password reset form with social login.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/bash-edit-profile-form/" target="_blank">Bash Edit Profile</a>: <?php _e( 'an elegant, responsive front-end edit profile form for WordPress.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/memories-edit-profile-form/" target="_blank">Memories edit profile form</a>: <?php _e( 'a clean and simple front-end edit profile form for WordPres.', 'profilepress' ); ?>
					</li>
					<li>
						<a href="http://profilepress.net/downloads/tag/perfecto/" target="_blank">Perfecto form</a>: <?php _e( 'a clean & polished login, registration and password reset form.', 'profilepress' ); ?>
					</li>
				</ul>
				<div><a href="http://profilepress.net/themes" target="_blank">
						<button class="button-primary" type="button"><?php _e( 'Shop Now', 'profilepress' ); ?></button>
					</a></div>
			</div>
		</div>
	</div>
</div>