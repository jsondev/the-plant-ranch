<div id="icon-options-general" class="icon32"></div>
<h2 class="nav-tab-wrapper">
	<a href="?page=pp-config" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-config' ? 'nav-tab-active' : ''; ?>">Settings</a>
	<a href="?page=pp-contact-info" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-contact-info' ? 'nav-tab-active' : ''; ?>">Contact Info</a>
	<a href="?page=pp-cpf" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-cpf' ? 'nav-tab-active' : ''; ?>">Profile Fields</a>
	<a href="?page=pp-login" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-login' ? 'nav-tab-active' : ''; ?>">Login</a>
	<a href="?page=pp-registration" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-registration' ? 'nav-tab-active' : ''; ?>">Registration</a>
	<a href="?page=pp-password-reset" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-password-reset' ? 'nav-tab-active' : ''; ?>">Password Reset</a>
	<a href="?page=pp-edit-profile" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-edit-profile' ? 'nav-tab-active' : ''; ?>">Edit Profile</a>
	<a href="?page=pp-user-profile" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-user-profile' ? 'nav-tab-active' : ''; ?>">User Profile</a>
	<a href="?page=pp-social-login" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-social-login' ? 'nav-tab-active' : ''; ?>">Social Login</a>
	<a href="?page=pp-extras" class="nav-tab <?php echo isset( $_GET['page'] ) && $_GET['page'] == 'pp-extras' ? 'nav-tab-active' : ''; ?>">Extras</a>
</h2>