<?php

require_once VIEWS . '/custom-profile-fields/extra-profile-field-bottom.php';

require_once 'custom-profile-fields-wp-list-table.php';

/**
 *  custom profile fields settings page
 */
class Custom_Profile_Fields_Settings_Page {
	private static $instance;
	private $profile_fields_form_errors;

	/** Constructor */
	function __construct() {
		add_action( 'admin_menu', array( $this, 'register_cpf_settings_page' ) );
		add_filter( 'set-screen-option', array( $this, 'save_screen_option' ), 10, 3 );
		add_action( 'admin_print_scripts', array( $this, 'js_confirm_custom_fields' ) );
	}

	function register_cpf_settings_page() {

		$hook = add_submenu_page( 'pp-config',
			__( 'Custom Profile Fields', 'profilepress' ) . ' - ProfilePress',
			__( 'Custom Profile Fields', 'profilepress' ),
			'manage_options',
			PROFILE_FIELDS_SETTINGS_PAGE_SLUG,
			array( $this, 'cpf_settings_page_function' ) );

		add_action( "load-$hook", array( $this, 'add_options' ) );
	}


	function add_options() {
		global $myListTable;
		$option = 'per_page';
		$args   = array(
			'label'   => 'Profile fields',
			'default' => 5,
			'option'  => 'profile_fields_per_page'
		);
		add_screen_option( $option, $args );

		$myListTable = new Profile_Fields_List_Table;

	}


	// save the screen option values
	function save_screen_option( $status, $option, $value ) {
		return $value;
	}

	// argument @operation determine if the method should save added field or edited field
	function save_add_edit_profile_fields( $operation, $id = '' ) {

		if ( isset( $_POST['add_new_field'] ) || isset( $_POST['edit_field'] ) ) {
			$label_name          = stripslashes( sanitize_text_field( $_POST['cpf_label_name'] ) );
			$key                 = sanitize_text_field( $_POST['cpf_key'] );
			$description         = stripslashes( sanitize_text_field( $_POST['cpf_description'] ) );
			$type                = sanitize_text_field( $_POST['cpf_type'] );
			$options             = esc_attr( $_POST['cpf_options'] );
			$is_multi_selectable = isset( $_POST['cpf_multi_select'] ) ? sanitize_text_field( $_POST['cpf_multi_select'] ) : '';

			// catch and save form generated errors in property @profile_fields_form_errors 
			if ( empty( $_POST['cpf_label_name'] ) ) {
				$this->profile_fields_form_errors = 'Field Label is empty';
			}
			elseif ( empty( $_POST['cpf_key'] ) ) {
				$this->profile_fields_form_errors = 'Field Key is empty';
			}
			elseif ( empty( $_POST['cpf_type'] ) ) {
				$this->profile_fields_form_errors = 'Please choose a form Type';
			}
			elseif ( $_POST['cpf_type'] == 'select' && empty( $_POST['cpf_options'] ) ) {
				$this->profile_fields_form_errors = 'Options field is required';
			}
			elseif ( $_POST['cpf_type'] == 'radio' && empty( $_POST['cpf_options'] ) ) {
				$this->profile_fields_form_errors = 'Options field is required';
			}

			if ( isset( $this->profile_fields_form_errors ) ) {
				return;
			}
		}

		if ( isset( $_POST['add_new_field'] ) && check_admin_referer( 'add_custom_profile_fields', '_wpnonce' ) && $operation == 'add' ) {
			global $wpdb;

			$insert = $wpdb->insert(
				$wpdb->base_prefix . 'pp_profile_fields',
				array(
					'label_name'  => $label_name,
					'field_key'   => $key,
					'description' => $description,
					'type'        => $type,
					'options'     => $options,
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',

				)
			);

			if ( $insert ) {

				if ( $type == 'select' && $is_multi_selectable == 'yes' ) {
				$old_data = get_option( 'pp_cpf_select_multi_selectable', array() );
					$new_data = array( $key => $id );
					update_option( 'pp_cpf_select_multi_selectable', array_unique( array_merge( $old_data, $new_data ) ) );
				}

				wp_redirect( '?page=' . PROFILE_FIELDS_SETTINGS_PAGE_SLUG . '&field-added=true' );
				exit;
			}
			else {
				echo 'Unexpected error. Please try again.';
			}

		}

		// check if the edit buttin was clicked and if nonces are ok
		if ( isset( $_POST['edit_field'] ) && check_admin_referer( 'edit_custom_profile_fields', '_wpnonce' ) ) {
			global $wpdb;

			// if field is being edited
			if ( $operation == 'edit' ) {

				$update = $wpdb->update(
					$wpdb->base_prefix . 'pp_profile_fields',
					array(
						'label_name'  => $label_name,
						'field_key'   => $key,
						'description' => $description,
						'type'        => $type,
						'options'     => $options,
					),
					array( 'id' => $id ),
					array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',

					),
					array( '%d' )
				);


				if ( $type == 'select' && 'yes' == $is_multi_selectable ) {
					$old_data = get_option( 'pp_cpf_select_multi_selectable', array() );
					$new_data = array( $key => $id );
					update_option( 'pp_cpf_select_multi_selectable', array_unique( array_merge( $old_data, $new_data ) ) );
				}
				else {
					$old_data = get_option( 'pp_cpf_select_multi_selectable', array() );
					unset( $old_data[ $key ] );
					update_option( 'pp_cpf_select_multi_selectable', array_unique( $old_data ) );

				}

				wp_redirect( '?page=' . PROFILE_FIELDS_SETTINGS_PAGE_SLUG . '&field-edited=true' );
				exit;


			}
		}


	}

	public function cpf_settings_page_function() {
		// if we are in addition of field page
		if ( isset( $_GET['post'] ) && $_GET['post'] == 'new' ) {
			?>
			<div class="wrap">
			<h2><?php _e('Custom Profile Fields', 'profilepress'); ?></h2>
			<?php
			// call function to insert field to db
			$this->save_add_edit_profile_fields( 'add' );

			// include the add new custom field file
			if ( isset( $this->profile_fields_form_errors ) ) {
				?>
				<div id="message" class="error notice is-dismissible"><p><strong><?php echo $this->profile_fields_form_errors; ?>. </strong>
					</p></div>
			<?php
			}
			require_once 'include.add-profile-fields-settings-page.php';
		} // if we are in field editing field
		elseif ( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
			?>
			<div class="wrap">
			<h2>Custom Profile Fields</h2>
			<?php
			// @GET field id to edit
			$field_id = esc_attr( (int) $_GET['field'] );

			// call function to edit field in db
			$this->save_add_edit_profile_fields( 'edit', $field_id ); ?>

			<?php
			// include the edit custom field file
			require_once 'include.edit-profile-fields-settings-page.php';
		} else {
			global $myListTable;

			echo '<div class="wrap"><h2>Custom Profile Fields <a class="add-new-h2" href="' . esc_attr( esc_url_raw( add_query_arg( 'post', 'new' ) ) ) . '">Add New</a></h2>';

			// status displayed when new field is added
			if ( isset( $_GET['field-edited'] ) && $_GET['field-edited'] ) {
				echo '<div id="message" class="updated notice is-dismissible"><p><strong>Profile Field Edited. </strong></p></div>';
			}

			// status displayed when new field is edited
			if ( isset( $_GET['field-added'] ) && $_GET['field-added'] ) {
				?>
				<div id="message" class="updated notice is-dismissible"><p><strong>New Profile Field Added. </strong></p></div>
			<?php
			}

			// include settings tab
			require_once VIEWS . '/include.settings-page-tab.php';

			$myListTable->prepare_items();
			?>

			<form method="post">
			<input type="hidden" name="page" value="ttest_list_table">
			<?php
			$myListTable->display();
			echo '</form></div>';
		}

	}


	/** Singleton instance */
	static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/** Add an alert before a custom_profile_field builder is deleted */
	public function js_confirm_custom_fields() {
		?>
		<script type="text/javascript">
			function pp_del_custom_fields(page, action, custom_fields, _wpnonce) {
				if (confirm("Are you sure you want to delete this?")) {
					window.location.href = '?page=' + page + '&action=' + action + '&field=' + custom_fields + '&_wpnonce=' + _wpnonce;
				}
			}
		</script>
		<?php
	}
}


Custom_Profile_Fields_Settings_Page::get_instance();