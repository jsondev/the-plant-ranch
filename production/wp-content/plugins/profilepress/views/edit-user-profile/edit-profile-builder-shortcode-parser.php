<?php

class Edit_User_Profile_Builder_Shortcode_Parser {

	static private $current_user;

	/**
	 * define all registration builder sub shortcode.
	 */
	function __construct() {

		add_action( 'init', array( $this, 'get_current_user' ) );

		add_shortcode( 'edit-profile-username', array( $this, 'edit_profile_username' ) );

		add_shortcode( 'edit-profile-password', array( $this, 'edit_profile_password' ) );

		add_shortcode( 'edit-profile-email', array( $this, 'edit_profile_email' ) );

		add_shortcode( 'edit-profile-website', array( $this, 'edit_profile_website' ) );

		add_shortcode( 'edit-profile-nickname', array( $this, 'edit_profile_nickname' ) );

		add_shortcode( 'edit-profile-display-name', array( $this, 'edit_profile_display_name' ) );

		add_shortcode( 'edit-profile-first-name', array( $this, 'edit_profile_first_name' ) );

		add_shortcode( 'edit-profile-last-name', array( $this, 'edit_profile_last_name' ) );

		add_shortcode( 'edit-profile-bio', array( $this, 'edit_profile_bio' ) );

		add_shortcode( 'remove-user-avatar', array( $this, 'remove_user_avatar' ) );

		add_shortcode( 'edit-profile-avatar', array( $this, 'edit_profile_avatar' ) );

		add_shortcode( 'edit-profile-cpf', array( $this, 'edit_profile_custom_profile_field' ) );

		add_shortcode( 'edit-profile-submit', array( $this, 'edit_profile_submit' ) );

		do_action( 'pp_register_password_reset_form_shortcode' );

	}


	/**
	 * Normalize un-named shortcode
	 *
	 * @param $atts
	 *
	 * @return mixed
	 */
	public static function normalize_attributes( $atts ) {
		foreach ( $atts as $key => $value ) {
			if ( is_int( $key ) ) {
				$atts[ $value ] = true;
				unset( $atts[ $key ] );
			}
		}

		return $atts;
	}

	/** Get the currently logged user */
	public function get_current_user() {

		$current_user = wp_get_current_user();
		if ( $current_user instanceof WP_User ) {
			self::$current_user = $current_user;
		}
	}

	/**
	 * parse the username shortcode
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	function edit_profile_username( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'Username',
				'required'    => '',
				'placeholder' => 'Username'
			),
			$atts
		);

		$atts = apply_filters( 'pp_edit_profile_username_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_username'] ) ? 'value="' . esc_attr( $_POST['eup_username'] ) . '"' : 'value="' . self::$current_user->user_login . '"';

		$title = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"eup_username\" type=\"text\" $title $value $class $id $placeholder $other_atts_html disabled='disabled' >";

		return apply_filters( 'pp_edit_profile_username_field', $html, $atts );
	}

	/**
	 * parse the password shortcode
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	function edit_profile_password( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'Password',
				'required'    => '',
				'placeholder' => 'Password'
			),
			$atts
		);

		$atts = apply_filters( 'pp_edit_profile_password_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_password'] ) ? 'value="' . esc_attr( $_POST['eup_password'] ) . '"' : 'value=""';
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"eup_password\" type='password' $title $value $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_edit_profile_password_field', $html, $atts );

	}


	/**
	 * Callback function for email
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_email( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'Email Address',
				'required'    => '',
				'placeholder' => 'Email Address'
			),
			$atts
		);

		$atts = apply_filters( 'pp_edit_profile_email_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_email'] ) ? 'value="' . esc_attr( $_POST['eup_email'] ) . '"' : 'value="' . self::$current_user->user_email . '"';
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"eup_email\" type=\"text\" $title $value $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_edit_profile_email_field', $html, $atts );

	}

	/**
	 * Callback function for website
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_website( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'Website',
				'required'    => '',
				'placeholder' => 'Website'
			),
			$atts
		);


		$atts = apply_filters( 'pp_edit_profile_website_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_website'] ) ? esc_attr( $_POST['eup_website'] ) : self::$current_user->user_url;
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"eup_website\" value='" . $value . "' type='text' $title $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_edit_profile_website_field', $html, $atts );

	}


	/**
	 * Callback function for nickname
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_nickname( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'Nickname',
				'required'    => '',
				'placeholder' => 'Nickname'
			),
			$atts
		);


		$atts = apply_filters( 'pp_edit_profile_nickname_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_nickname'] ) ? esc_attr( $_POST['eup_nickname'] ) : self::$current_user->nickname;
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name='eup_nickname' value='" . $value . "' type='text' $title $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_edit_profile_nickname_field', $html, $atts );

	}


	/**
	 * Callback for display name
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_display_name( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'    => '',
				'id'       => '',
				'title'    => 'Display Name',
				'required' => ''
			),
			$atts
		);


		$atts = apply_filters( 'pp_edit_profile_display_name_field_atts', $atts );

		$title    = 'title="' . $atts['title'] . '"';
		$class    = 'class="' . $atts['class'] . '"';
		$id       = 'id="' . $atts['id'] . '"';
		$required = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';


		$profileuser                        = wp_get_current_user();
		$public_display                     = array();
		$public_display['display_nickname'] = $profileuser->nickname;
		$public_display['display_username'] = $profileuser->user_login;

		if ( ! empty( $profileuser->first_name ) ) {
			$public_display['display_firstname'] = $profileuser->first_name;
		}

		if ( ! empty( $profileuser->last_name ) ) {
			$public_display['display_lastname'] = $profileuser->last_name;
		}

		if ( ! empty( $profileuser->first_name ) && ! empty( $profileuser->last_name ) ) {
			$public_display['display_firstlast'] = $profileuser->first_name . ' ' . $profileuser->last_name;
			$public_display['display_lastfirst'] = $profileuser->last_name . ' ' . $profileuser->first_name;
		}

		if ( ! in_array(
			$profileuser->display_name,
			$public_display )
		) // Only add this if it isn't duplicated elsewhere
		{
			$public_display = array( 'display_displayname' => $profileuser->display_name ) + $public_display;
		}

		$public_display = array_map( 'trim', $public_display );
		$public_display = array_unique( $public_display );

		$html = "<select name=\"eup_display_name\" $title $id $class $other_atts_html $required >";

		foreach ( $public_display as $id => $item ) {
			$selected = selected( $profileuser->display_name, $item, false );
			$html .= "<option $selected> $item </option>";
		}

		$html .= '</select>';

		return apply_filters( 'pp_edit_profile_display_name_field', $html, $atts );

	}


	/**
	 * Callback function for first name
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_first_name( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'First Name',
				'required'    => '',
				'placeholder' => 'First Name'
			),
			$atts
		);

		$atts = apply_filters( 'pp_edit_profile_first_name_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_first_name'] ) ? esc_attr( $_POST['eup_first_name'] ) : self::$current_user->first_name;
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';

		$title = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"eup_first_name\" type='text' value='" . $value . "' $title $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_edit_profile_first_name_field', $html, $atts );

	}


	/**
	 * Callback for last name
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_last_name( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'Last Name',
				'required'    => '',
				'placeholder' => 'Last Name'
			),
			$atts
		);

		$atts = apply_filters( 'pp_edit_profile_last_name_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_last_name'] ) ? esc_attr( $_POST['eup_last_name'] ) : self::$current_user->last_name;
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';

		$title = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"eup_last_name\" value=\"$value\" type=\"text\" $title $class $placeholder $id $other_atts_html $required >";

		return apply_filters( 'pp_edit_profile_last_name_field', $html, $atts );

	}


	/**
	 * Handles user BIO
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_bio( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => 'Biographical Information',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);


		$atts = apply_filters( 'pp_edit_profile_bio_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = ! empty( $atts['placeholder'] ) ? $atts['placeholder'] : '';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['eup_bio'] ) ? esc_textarea( $_POST['eup_bio'] ) : self::$current_user->description;
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';

		$title = 'title="' . $atts['title'] . '"';

		$html = "<textarea name=\"eup_bio\" $title $class placeholder=\"$placeholder\" $id $required $other_atts_html >$value</textarea>";

		return apply_filters( 'pp_edit_profile_bio_field', $html, $atts );

	}


	public function edit_profile_avatar( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);


		$atts = apply_filters( 'pp_edit_profile_avatar_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';

		$field_title = $atts['title'];
		$title       = isset( $field_title ) ? "title=\"$field_title\"" : '';

		$html = "<input name=\"eup_avatar\" type=\"file\" $title $class $placeholder $id $other_atts_html $required />";

		return apply_filters( 'pp_edit_profile_avatar_field', $html, $atts );
	}


	/**
	 * Handle custom profile fields
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	function edit_profile_custom_profile_field( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'title'       => '',
				'required'    => '',
				'key'         => '',
				'type'        => '',
				'placeholder' => '',
				'limit' => ''
			),
			$atts
		);


		$atts = apply_filters( 'pp_edit_profile_cpf_field_atts', $atts );

		$key   = $atts['key'];
		$type  = $atts['type'];
		$class = 'class="' . $atts['class'] . '"';
		$id    = 'id="' . $atts['id'] . '"';
		$title = 'title="' . $atts['title'] . '"';

		if ( empty( $key ) || empty( $type ) ) {
			return __( 'Field key and/or type is missing', 'profilepress' );
		}

		if ( $type == 'select' ) {
			$multiple          = pp_is_select_field_multi_selectable( $key ) ? 'multiple' : null;
			$chosen_class_name = isset( $multiple ) && $multiple == 'multiple' ? 'pp_chosen ' : null;
			$class             = 'class="' . $chosen_class_name . $atts['class'] . '"';
			$placeholder       = 'data-placeholder="' . $atts['placeholder'] . '"';

			$select_tag_key = isset( $multiple ) && $multiple == 'multiple' ? "{$key}[]" : $key;
			$html           = "<select name=\"$select_tag_key\" $placeholder $class $id $other_atts_html $multiple>";


			// get select option values
			$option_values = PROFILEPRESS_sql::get_field_option_values( $key );

			// explode the options to an array
			if ( isset( $option_values[0] ) ) {
				$option_values = explode( ',', $option_values[0] );

				foreach ( $option_values as $value ) {
					$value = trim( $value );

					// selected for <select>
					if ( is_array( @$_POST[ $key ] ) && in_array( $value, @$_POST[ $key ] ) ) {
						$selected = 'selected="selected"';
					}
					// !isset($_POST[ $key ] is called to not run the succeeding code if the form is submitted.
					// to enable the select dropdown retain the submitted options when an error occur/ prevent the form from saving.
					elseif ( !isset($_POST[ $key ]) && is_array( self::$current_user->$key ) && in_array( $value, self::$current_user->$key ) ) {
						$selected = 'selected="selected"';
					}
					elseif ( !isset($_POST[ $key ]) && ! is_array( self::$current_user->$key ) && $value == self::$current_user->$key ) {
						$selected = 'selected="selected"';
					}
					// check for non array meta values
					else {
						$selected = null;
					}

					$html .= "<option value=\"$value\" $selected>$value</option>";
				}
			}

			$html .= '</select>';
			if ( isset( $multiple ) && $multiple == 'multiple' ) {
				$limit       =     absint($atts['limit']);
				$limit = (isset($limit)) ? "{max_selected_options: $limit}" : null;

				$html .= <<<SCRIPT
<script>
  jQuery(function($) {
    $( ".pp_chosen" ).chosen($limit);
  });
  </script>
SCRIPT;

			}
		}


		// if we are dealing with a radio button
		if ( $type == 'radio' ) {

			// get select option values
			$option_values = PROFILEPRESS_sql::get_field_option_values( $key );

			// explode the options to an array
			$option_values = explode( ',', $option_values[0] );

			$html = '';
			foreach ( $option_values as $value ) {
				$value = trim( $value );

				// checked for radio buttons
				$checked = @checked(
					isset( $_POST[ $key ] ) && ! empty( $_POST[ $key ] ) ? $_POST[ $key ] : self::$current_user->$key,
					$value,
					false );

				$html .= "<input type=\"radio\" name=\"$key\" value=\"$value\" id=\"$value\" $class $title $checked $other_atts_html />";
				$html .= "<label class=\"profilepress-reg-label css-labelz\" for=\"$value\"> $value </label>";
			}
		}

		if ( $type == 'checkbox' ) {

			// get select option values
			$option_values = PROFILEPRESS_sql::get_field_option_values( $key );

			// explode the options to an array
			$option_values = explode( ',', $option_values[0] );

			$html = '';
			foreach ( $option_values as $value ) {
				$value = trim( $value );

				// checked for radio buttons
				$checked = @checked(
					isset( $_POST[ $key ] ) && ! empty( $_POST[ $key ] ) ? $_POST[ $key ] : self::$current_user->$key,
					$value,
					false
				);

				$html .= "<input type=\"checkbox\" name=\"$key\" value=\"$value\" id=\"$value\" $title $class $checked $other_atts_html/>";
				$html .= "<label for=\"$value\"> $value </label>";
			}
		}
		// if we are dealing with a text
		if ( $type == 'text' ) {

			$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
			$value       = isset( $_POST[ $key ] ) ? 'value="' . esc_attr( $_POST[ $key ] ) . '"' : 'value="' . self::$current_user->$key . '"';

			$html = "<input name='" . $key . "' type='text' $value $class $id $placeholder $title />";

		}

		// if we are dealing with a textarea
		if ( $type == 'textarea' ) {

			$field_title = $atts['title'];
			$title       = "title=\"$field_title\"";

			$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
			$value       = isset( $_POST[ $key ] ) ? esc_attr( $_POST[ $key ] ) : self::$current_user->$key;

			$html = "<textarea name=\"$key\" $title $class $placeholder $id $other_atts_html>$value</textarea>";

		}


		// if we are dealing with a text
		if ( $type == 'date' ) {

			$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
			$value       = isset( $_POST[ $key ] ) ? 'value="' . esc_attr( $_POST[ $key ] ) . '"' : 'value="' . self::$current_user->$key . '"';
			$class       = 'class="pp_datepicker ' . $atts['class'] . '"';

			$html = "<input name='" . $key . "' type='text' $value $class $id $placeholder $title />";
			$html .= <<<SCRIPT
<script>
  jQuery(function($) {
    $( ".pp_datepicker" ).datepicker();
  });
  </script>
SCRIPT;

		}

		if ( 'file' == $type ) {
			$html             = '';
			$user_upload_data = get_user_meta( self::$current_user->ID, 'pp_uploaded_files', true );
			// if the user uploads isn't empty and there exist a file with the custom field key.
			if ( ! empty( $user_upload_data ) && $filename = @$user_upload_data[ $key ] ) {
				$link = PP_FILE_UPLOAD_URL . $filename;

				$html = "<div class='pp-user-upload'><a href='$link'>$filename</a></div>";
			}
			else {
				$html = '<div class="pp-user-upload">' . __( 'No file was found.', 'profilepress' ) . '</div>';
			}

			$html = apply_filters( 'pp_edit_profile_hide_file', $html );

			// $value $class $id $placeholder $title
			$html .= "<input name='" . $key . "' type='file' $title $class $id $other_atts_html>";
		}

		return apply_filters( 'pp_edit_profile_cpf_field', $html, $atts );

	}


	/**
	 * Remove a user avatar
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public
	function remove_user_avatar(
		$atts
	) {

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class' => '',
				'id'    => '',
				'title' => '',
				'label' => __( 'Delete Avatar', 'profilepress' )
			),
			$atts
		);

		$atts = apply_filters( 'pp_edit_profile_remove_avatar_button_atts', $atts );

		$class = 'class="' . $atts['class'] . '"';
		$label = ! empty( $atts['label'] ) ? $atts['label'] : null;
		$id    = 'id="' . $atts['id'] . '"';
		$title = 'title="' . $atts['title'] . '"';

		// ensure a profile avatar for the user is available before the remove button gets displayed
		$avatar_slug = get_user_meta( self::$current_user->ID, 'pp_profile_avatar', true );
		if ( ! empty( $avatar_slug ) ) {
			$button = "<button type=\"submit\" id=\"pp-del-avatar\" name=\"eup_remove_avatar\" value=\"removed\" $class $id $title $other_atts_html>$label</button>";

			return apply_filters( 'pp_edit_profile_remove_avatar_button', $button, $atts );
		}
	}


	/**
	 * Callback function for submit button
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public
	function edit_profile_submit(
		$atts
	) {

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class' => '',
				'name'  => 'eup_submit',
				'id'    => '',
				'value' => 'Save Changes',
				'title' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_edit_profile_submit_field_atts', $atts );

		$name  = 'name="' . $atts['name'] . '"';
		$class = 'class="' . $atts['class'] . '"';
		$value = 'value="' . $atts['value'] . '"';
		$id    = 'id="' . $atts['id'] . '"';

		$title = 'title="' . $atts['title'] . '"';

		$html = "<input type='submit' $name $title $value $id $class $other_atts_html />";

		return apply_filters( 'pp_edit_profile_submit_field', $html, $atts );
	}


	/** Singleton instance */
	static function get_instance() {
		static $instance = false;

		if ( ! $instance ) {
			$instance = new self;
		}

		return $instance;
	}
}

Edit_User_Profile_Builder_Shortcode_Parser::get_instance();
