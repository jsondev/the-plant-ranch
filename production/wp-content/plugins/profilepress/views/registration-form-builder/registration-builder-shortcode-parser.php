<?php

class Registration_Builder_Shortcode_Parser {

	/**
	 * define all registration builder sub shortcode.
	 */
	public static function initialize() {
		add_shortcode( 'reg-username', array( __CLASS__, 'reg_username' ) );
		add_shortcode( 'reg-password', array( __CLASS__, 'reg_password' ) );
		add_shortcode( 'reg-confirm-password', array( __CLASS__, 'reg_confirm_password' ) );
		add_shortcode( 'reg-email', array( __CLASS__, 'reg_email' ) );
		add_shortcode( 'reg-website', array( __CLASS__, 'reg_website' ) );
		add_shortcode( 'reg-nickname', array( __CLASS__, 'reg_nickname' ) );
		add_shortcode( 'reg-display-name', array( __CLASS__, 'reg_display_name' ) );
		add_shortcode( 'reg-first-name', array( __CLASS__, 'reg_first_name' ) );
		add_shortcode( 'reg-last-name', array( __CLASS__, 'reg_last_name' ) );
		add_shortcode( 'reg-bio', array( __CLASS__, 'reg_bio' ) );
		add_shortcode( 'reg-avatar', array( __CLASS__, 'reg_avatar' ) );
		add_shortcode( 'reg-cpf', array( __CLASS__, 'reg_custom_profile_field' ) );
		add_shortcode( 'reg-submit', array( __CLASS__, 'reg_submit' ) );
		add_shortcode( 'reg-password-meter', array( __CLASS__, 'password_meter' ) );

		do_action( 'pp_register_registration_form_shortcode' );
	}


	/**
	 * Normalize unamed shortcode
	 *
	 * @param array $atts
	 *
	 * @return mixed
	 */
	public static function normalize_attributes( $atts ) {
		if ( is_array( $atts ) ) {
			foreach ( $atts as $key => $value ) {
				if ( is_int( $key ) ) {
					$atts[ $value ] = true;
					unset( $atts[ $key ] );
				}
			}
		}

		return $atts;
	}


	/**
	 * parse the [login-username] shortode
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function reg_username( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => true,
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_username_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_username'] ) ? 'value="' . esc_attr( $_POST['reg_username'] ) . '"' : 'value="' . $atts['value'] . '"';
		$required    = isset( $atts['required'] ) && ( $atts['required'] === true || $atts['required'] == 'true' ) ? 'required="required"' : null;

		$title = 'title="' . $atts['title'] . '"';

		$html = "<input name='reg_username' type='text' $title $value $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_username_field', $html, $atts );
	}

	/**
	 * @param array $atts
	 *
	 * parse the [login-password] shortcode
	 *
	 * @return string
	 */
	public static function reg_password( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => true,
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_password_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_password'] ) ? 'value="' . esc_attr( $_POST['reg_password'] ) . '"' : 'value="' . $atts['value'] . '"';
		$required    = isset( $atts['required'] ) && ( $atts['required'] === true || $atts['required'] == 'true' ) ? 'required="required"' : null;
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"reg_password\" type='password' $title $value $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_password_field', $html, $atts );

	}

	/**
	 * @param array $atts
	 *
	 * parse the [login-password] shortcode
	 *
	 * @return string
	 */
	public static function reg_confirm_password( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => true,
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_confirm_password_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_password2'] ) ? 'value="' . esc_attr( $_POST['reg_password2'] ) . '"' : 'value="' . $atts['value'] . '"';
		$required    = isset( $atts['required'] ) && ( $atts['required'] === true || $atts['required'] == 'true' ) ? 'required="required"' : null;
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"reg_password2\" type='password' $title $value $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_confirm_password_field', $html, $atts );


	}


	/**
	 * Callback function for email
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_email( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => true,
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_email_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_email'] ) ? 'value="' . esc_attr( $_POST['reg_email'] ) . '"' : 'value="' . $atts['value'] . '"';
		$required    = isset( $atts['required'] ) && ( $atts['required'] === true || $atts['required'] == 'true' ) ? 'required="required"' : null;
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name='reg_email' type='email' $title $value $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_email_field', $html, $atts );

	}


	/**
	 * Callback function for website
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_website( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_website_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_website'] ) ? esc_attr( $_POST['reg_website'] ) : $atts['value'];
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name='reg_website' value='" . $value . "' type='text' $title $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_website_field', $html, $atts );

	}


	/**
	 * Callback function for nickname
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_nickname( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_nickname_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_nickname'] ) ? esc_attr( $_POST['reg_nickname'] ) : $atts['value'];
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name='reg_nickname' value='" . $value . "' type='text' $title $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_nickname_field', $html, $atts );

	}

	/**
	 * Callback function for nickname
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_display_name( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_display_name_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_display_name'] ) ? esc_attr( $_POST['reg_display_name'] ) : $atts['value'];
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name='reg_display_name' value='" . $value . "' type='text' $title $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_display_name_field', $html, $atts );

	}


	/**
	 * Callback function for first name
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_first_name( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_first_name_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_first_name'] ) ? esc_attr( $_POST['reg_first_name'] ) : $atts['value'];
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';

		$title = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"reg_first_name\" type='text' value='" . $value . "' $title $class $id $placeholder $other_atts_html $required >";

		return apply_filters( 'pp_registration_first_name_field', $html, $atts );

	}


	/**
	 * Callback for last name
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_last_name( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_last_name_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_last_name'] ) ? esc_attr( $_POST['reg_last_name'] ) : $atts['value'];
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"reg_last_name\" value=\"$value\" type=\"text\" $title $class $placeholder $id $other_atts_html $required >";

		return apply_filters( 'pp_registration_last_name_field', $html, $atts );

	}


	/**
	 * Handles BIO
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_bio( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_bio_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$value       = isset( $_POST['reg_bio'] ) ? esc_textarea( $_POST['reg_bio'] ) : $atts['value'];
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		$title       = 'title="' . $atts['title'] . '"';

		$html = "<textarea name=\"reg_bio\" $title $class $placeholder $id $other_atts_html $required>$value</textarea>";

		return apply_filters( 'pp_registration_bio_field', $html, $atts );

	}


	/** Upload avatar field */
	public static function reg_avatar( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'placeholder' => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_avatar_field_atts', $atts );

		$class       = 'class="' . $atts['class'] . '"';
		$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
		$id          = 'id="' . $atts['id'] . '"';
		$required    = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';

		$title = 'title="' . $atts['title'] . '"';

		$html = "<input name=\"reg_avatar\" type=\"file\" $title $class $placeholder $id $other_atts_html $required >";

		return apply_filters( 'pp_registration_avatar_field', $html, $atts );
	}


	/**
	 * Handle custom registration | profile fields
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_custom_profile_field( $atts ) {

		$atts = self::normalize_attributes( $atts );

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'       => '',
				'id'          => '',
				'value'       => '',
				'title'       => '',
				'required'    => '',
				'key'         => '',
				'type'        => '',
				'placeholder' => '',
				'limit'    => ''
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_cpf_field_atts', $atts );

		$key      = $atts['key'];
		$type     = $atts['type'];
		$class    = 'class="' . $atts['class'] . '"';
		$id       = 'id="' . $atts['id'] . '"';
		$required = ( ! empty( $atts['required'] ) && $atts['required'] ) ? 'required="required"' : '';
		if ( empty( $key ) || empty( $type ) ) {
			return __( 'Field key or type is missing', 'profilepress' );
		}
		elseif ( $type == 'select' ) {

			$multiple          = pp_is_select_field_multi_selectable( $key ) ? 'multiple' : null;
			$chosen_class_name = isset( $multiple ) && $multiple == 'multiple' ? 'pp_chosen ' : null;
			$class             = 'class="' . $chosen_class_name . $atts['class'] . '"';
			$placeholder       = 'data-placeholder="' . $atts['placeholder'] . '"';

			$select_tag_key = isset( $multiple ) && $multiple == 'multiple' ? "{$key}[]" : $key;
			$html           = "<select name=\"$select_tag_key\" $placeholder $class $id $other_atts_html $required $multiple>";


			// get select option values
			$option_values = PROFILEPRESS_sql::get_field_option_values( $key );

			// explode the options to an array
			if ( isset( $option_values[0] ) ) {
				$option_values = explode( ',', $option_values[0] );

				foreach ( $option_values as $value ) {
					$value = trim( $value );

					// selected for <select>
					$selected = is_array( @$_POST[ $key ] ) && in_array( $value, @$_POST[ $key ] ) ? 'selected="selected"' : @selected( @$_POST[ $key ], $value, false );
					$html .= "<option value=\"$value\" $selected>$value</option>";
				}
			}

			$html .= '</select>';
			if ( isset( $multiple ) && $multiple == 'multiple' ) {
				$limit       =     absint($atts['limit']);
				$limit = (isset($limit)) ? "{max_selected_options: $limit}" : null;
				$html .= <<<SCRIPT
<script>
  jQuery(function($) {
    $( ".pp_chosen" ).chosen($limit);
  });
  </script>
SCRIPT;
			}
		} // if we are dealing with a radio button
		elseif ( $type == 'radio' ) {

			// get select option values
			$option_values = PROFILEPRESS_sql::get_field_option_values( $key );

			// explode the options to an array
			$option_values = explode( ',', $option_values[0] );

			$html = '';
			foreach ( $option_values as $value ) {
				$value = trim( $value );

				// checked for radio buttons
				$checked = @checked( $_POST[ $key ], $value, false );

				$html .= "<input type='radio' name=\"$key\" value=\"$value\" id=\"$value\" $class $checked $other_atts_html $required>";
				$html .= "<label class=\"profilepress-reg-label css-labelz\" for=\"$value\">$value</label>";
			}
		}
		elseif ( $type == 'checkbox' ) {

			// get select option values
			$option_values = PROFILEPRESS_sql::get_field_option_values( $key );

			// explode the options to an array
			$option_values = explode( ',', $option_values[0] );

			$html = '';
			foreach ( $option_values as $value ) {
				$value = trim( $value );

				// checked for radio buttons
				$checked = @checked( $_POST[ $key ], $value, false );

				$html .= "<input type='checkbox' name=\"$key\" value=\"$value\" id=\"$value\" $class $checked $other_atts_html $required>";
				$html .= "<label for=\"$value\">$value</label>";
			}
		} // if we are dealing with a text
		elseif ( $type == 'text' ) {
			$value_attr = $atts['value'];

			$field_title = $atts['title'];
			$title       = "title=\"$field_title\"";

			$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
			$value       = isset( $_POST[ $key ] ) ? 'value="' . esc_attr( $_POST[ $key ] ) . '"' : 'value="' . $value_attr . '"';

			$html = "<input name='" . $key . "' type='text' $title $value $class $id $placeholder $other_atts_html $required>";

		} // if we are dealing with a textarea
		elseif ( $type == 'textarea' ) {
			$value_attr = $atts['value'];

			$field_title = $atts['title'];
			$title       = "title=\"$field_title\"";

			$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
			$value       = isset( $_POST[ $key ] ) ? esc_attr( $_POST[ $key ] ) : $value_attr;

			$html = "<textarea name=\"$key\" $title $class $placeholder $id $other_atts_html $required>$value</textarea>";

		} // if we are dealing with a date
		elseif ( $type == 'date' ) {
			$value_attr = $atts['value'];

			$field_title = $atts['title'];
			$title       = "title=\"$field_title\"";
			$class       = 'class="pp_datepicker ' . $atts['class'] . '"';

			$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
			$value       = isset( $_POST[ $key ] ) ? 'value="' . esc_attr( $_POST[ $key ] ) . '"' : 'value="' . $value_attr . '"';

			$html = "<input name='" . $key . "' type='text' $title $value $class $id $placeholder $other_atts_html $required>";
			$html .= <<<SCRIPT
<script>
  jQuery(function($) {
    $( ".pp_datepicker" ).datepicker();
  });
  </script>
SCRIPT;

		}
		elseif ( 'file' == $type ) {

			$field_title = $atts['title'];
			$title       = "title=\"$field_title\"";

			$placeholder = 'placeholder="' . $atts['placeholder'] . '"';
			$value       = isset( $_POST[ $key ] ) ? 'value="' . esc_attr( $_POST[ $key ] ) . '"' : 'value=""';

			$html = "<input name='" . $key . "' type='file' $title $value $class $id $placeholder $other_atts_html $required>";
			// if field is required, add an hidden field
			if ( $atts['required'] ) {
				$html .= "<input name='required-" . $key . "' type='hidden' value='true'>";
			}
		}
		else {
			$html = __( 'custom field not defined', 'profilepress' );
		}

		return apply_filters( 'pp_registration_cpf_field', $html, $atts );

	}


	/**
	 * Callback function for submit button
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function reg_submit( $atts ) {

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class' => '',
				'name'  => 'reg_submit',
				'id'    => '',
				'value' => __( 'Sign Up', 'profilepress' ),
				'title' => '',
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_submit_field_atts', $atts );

		$name  = 'name="' . $atts['name'] . '"';
		$class = 'class="' . $atts['class'] . '"';
		$value = 'value="' . $atts['value'] . '"';
		$id    = 'id="' . $atts['id'] . '"';
		$title = 'title="' . $atts['title'] . '"';

		$html = "<input type='submit' $name $title $value $id $class $other_atts_html >";

		return apply_filters( 'pp_registration_submit_field', $html, $atts );
	}


	/**
	 * Password strength meter field.
	 * @see http://code.tutsplus.com/articles/using-the-included-password-strength-meter-script-in-wordpress--wp-34736
	 */
	public static function password_meter( $atts ) {

		// grab unofficial attributes
		$other_atts_html = pp_other_field_atts( $atts );

		$atts = shortcode_atts(
			array(
				'class'   => '',
				'enforce' => 'true',
			),
			$atts
		);

		$atts = apply_filters( 'pp_registration_password_meter_field_atts', $atts );

		wp_localize_script( 'password-strength-meter', 'pwsL10n', array(
			'empty'    => __( 'Strength indicator' ),
			'short'    => __( 'Very weak' ),
			'bad'      => __( 'Weak' ),
			'good'     => _x( 'Medium', 'password strength' ),
			'strong'   => __( 'Strong' ),
			'mismatch' => __( 'Mismatch' )
		) );

		ob_start(); ?>

		<?php if ( 'true' == $atts['enforce'] ) : ?>
			<input type="hidden" name="pp_enforce_password_meter" value="true">
		<?php endif; ?>
		<div id="pp-pass-strength-result" <?php echo 'class="' . $atts['class'] . '"' . $other_atts_html; ?>><?php _e( 'Strength indicator' ); ?></div>
		<script type="text/javascript">
			var pass_strength = 0;
			jQuery(document).ready(function ($) {
				var password1 = $('input[name=reg_password]');
				var password2 = $('input[name=reg_password2]');
				var submitButton = $('input[name=reg_submit]');
				var strengthMeterId = $('#pp-pass-strength-result');

				$('body').on('keyup', 'input[name=reg_password], input[name=reg_password2]',
					function (event) {
						pp_checkPasswordStrength(password1, password2, strengthMeterId, submitButton, []);
					}
				);

				if (password1.val() != '') {
					// trigger 'keyup' event to check password strength when password field isn't empty.
					$('body input[name=reg_password]').trigger('keyup');
				}

				submitButton.click(function () {
					$('input[name=pp_enforce_password_meter]').val(pass_strength);
				});
			});

			function pp_checkPasswordStrength($pass1, $pass2, $strengthResult, $submitButton, blacklistArray) {
				var pass1 = $pass1.val();
				var pass2 = $pass2.val();

				<?php if('true' == $atts['enforce']) : ?>
				// Reset the form & meter
				$submitButton.attr('disabled', 'disabled').css("opacity", ".4");
				<?php endif; ?>
				$strengthResult.removeClass('short bad good strong');

				// Extend our blacklist array with those from the inputs & site data
				blacklistArray = blacklistArray.concat(wp.passwordStrength.userInputBlacklist())

				// Get the password strength
				var strength = wp.passwordStrength.meter(pass1, blacklistArray, pass2);

				// Add the strength meter results
				switch (strength) {

					case 2:
						$strengthResult.addClass('bad').html(pwsL10n.bad);
						break;
					case 3:
						$strengthResult.addClass('good').html(pwsL10n.good);
						break;
					case 4:
						$strengthResult.addClass('strong').html(pwsL10n.strong);
						pass_strength = 1;
						break;
					case 5:
						$strengthResult.addClass('short').html(pwsL10n.mismatch);
						break;
					default:
						$strengthResult.addClass('short').html(pwsL10n.short);
				}

				// The meter function returns a result even if pass2 is empty,
				// enable only the submit button if the password is strong
				<?php if('true' == $atts['enforce']) : ?>
				if (4 === strength) {
					$submitButton.removeAttr('disabled').css("opacity", "");
				}
				<?php endif; ?>

				return strength;
			}
		</script>
		<?php
		return apply_filters( 'pp_registration_password_meter_field', ob_get_clean(), $atts );
	}
}

Registration_Builder_Shortcode_Parser::initialize();