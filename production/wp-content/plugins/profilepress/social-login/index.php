<?php
/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2014, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */

// ------------------------------------------------------------------------
//	HybridAuth End Point
// ------------------------------------------------------------------------

// Boostrap WP - Pantheon hosting
if (defined( "PANTHEON_BINDING" )) {
	include_once( "/srv/bindings/". PANTHEON_BINDING ."/code/wp-blog-header.php" );
}

require_once( "Hybrid/Auth.php" );
require_once( "Hybrid/Endpoint.php" );

Hybrid_Endpoint::process();

Hybrid_Provider_Adapter::logout();