(function ($) {
    $(document).ready(function () {
        $('#pp-del-avatar').click(function (e) {

            // button text
            var button_text = $(this).text();
            // object of this(button clicked)
            var this_obj = $(this);

            e.preventDefault();
            swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover it.',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    closeOnConfirm: false
                },
                function () {
                    swal.disableButtons();

                    $(this_obj).text('Deleting...');
                    $.post(pp_del_avatar_obj.ajaxurl, {
                        action: 'pp_del_avatar',
                        nonce: pp_del_avatar_obj.nonce
                    }).done(function (data) {
                        if (data.error && data.error == 'nonce_failed') {
                            $(this_obj).text(button_text);
                            swal('An error occurred, please try again');
                        }
                        else if (data.success) {
                            $("img[data-del='avatar']").attr('src', data.default);
                            $(this_obj).remove();
                            swal(
                                'Deleted!',
                                'Profile picture has been deleted.',
                                'success'
                            );
                        }
                    });

                });
        });
    });
})(jQuery);